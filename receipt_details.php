<?php session_start();  
include 'config/connection.php';

if (isset($_GET['id'])) {

  $doc_no = $_GET['id'];
  $run_date = $_SESSION['rec_date'];

  $qry = "SELECT DISTINCT(op_code),sys_user.name,till_no,pos_trans_details.input_date from pos_trans_details LEFT JOIN sys_user ON sys_user.operator_code = pos_trans_details.op_code WHERE doc_no = '$doc_no' and run_date='$run_date' UNION SELECT DISTINCT(op_code),sys_user.name,till_no,till_trans_details.input_date from till_trans_details LEFT JOIN sys_user ON sys_user.operator_code = till_trans_details.op_code WHERE doc_no = '$doc_no' and run_date='$run_date'";
  $result = pg_query($conn, $qry) or die(pg_last_error($conn)); 
}



?>

<!DOCTYPE html>
 <html>
 <head>
   <meta charset="utf-8">
   <title>Receipt Details</title>

 <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

   
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<style type="text/css">
  .rec{
    margin-left: 10rem;
    margin-top: 1rem;
    width: 80%;
  }
  h3{
    text-align: center;
  }
  input{
    font-size: medium;
    padding: 0px;
    background-color: #e5e4e2;
    border-width: 1px;
    border-color: #c0c0c0;
    font-weight: lighter;
  }
</style>

 
 </head>
<body class="hold-transition text-sm" style="background-color: #dcdcdc;">
<div class="wrapper">

 
<!-- Content Wrapper. Contains page content -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="rec">
          <h3 class="text-sm">Receipt Details</h3>
            <div class="card">
              <div class="card-header">
                <?php $rw =pg_fetch_assoc($result); ?>
                Doc No: 
                <input type="text" name="docno" value="<?php echo $doc_no ?>" readonly>
                Till :
                <input type="text" name="tillno" value="<?php echo $rw['till_no'] ?>" style="width: 50px" readonly>
                Cashier: 
                <input type="text" name="opcode" value="<?php echo $rw['op_code'] ?>" style="width: 50px" readonly>
                <input type="text" name="cashier" value="<?php echo $rw['name'] ?>" readonly>
                <div class="float-right">
                  Time: 
                  <input type="text" name="time" value="<?php echo $rw['input_date'] ?>" readonly>
                </div>
              </div>
              <div class="card-body">
                <table class="table table-sm table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <td>Doc No:</td>
                      <td>Time:</td>
                      <td>#</td>
                      <td>Code:</td>
                      <td>Description:</td>
                      <td>Qty:</td>
                      <td>UOM:</td>
                      <td>Unit Price</td>
                      <td>Disc:</td>
                      <td>Line Total:</td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $query = "SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,pos_trans_details.input_date FROM pos_trans_details LEFT JOIN packing on pos_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date' UNION SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,till_trans_details.input_date FROM till_trans_details LEFT JOIN packing on till_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date'";
                    $results = pg_query($conn, $query) or die($query);
                    $i = 1;
                    while ($row = pg_fetch_array($results)) { ?>
                    <tr>
                      <td><?php echo $row['doc_no']; ?></td>
                      <td><?php echo $row['input_date']; ?></td>
                      <td><?php echo $i++ ?></td>
                      <td><?php echo $row['code']; ?></td>
                      <td><?php echo $row['description']; ?></td>
                      <td><?php echo number_format($row['qty'],2); ?></td>
                      <td><?php echo $row['uom']; ?></td>
                      <td><?php echo number_format($row['unit_incl'],2); ?></td>
                      <td><?php echo number_format($row['discount'],2); ?></td>
                      <td><?php echo number_format($row['total_incl'],2); ?></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                
                <a href="print_rec.php?id=<?php echo $doc_no ?>" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                <button class="btn btn-info btn-sm"><i class="fas fa-file-export"></i><a href="#" style="color: black;">Export</a></button>
                <button class="btn btn-danger btn-sm"><i class="fa-thin fa-cancel"></i><a href="index.php?page=receipt_listing" style="color: black;">Close</a></button>
                <div class="float-right">
                  <?php $rows = pg_num_rows($results); ?>
                  Total Items: 
                  <input type="text" name="items" style="width: 60px;" value="<?php echo number_format($rows,2) ?>" readonly>
                  <?php 
                  $qury = "SELECT sum(a.total_incl) as total FROM(SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,pos_trans_details.input_date FROM pos_trans_details LEFT JOIN packing on pos_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date' UNION SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,till_trans_details.input_date FROM till_trans_details LEFT JOIN packing on till_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date') as a";
                  $rslt = pg_query($conn, $qury) or die($query);
                  $sum = pg_fetch_assoc($rslt);
                   ?>
                  Total Amount: 
                  <input type="text" name="" value="<?php echo number_format($sum['total'],2)?>" style="text-align: right;" readonly>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  
 </body>
 <!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->

 </html>