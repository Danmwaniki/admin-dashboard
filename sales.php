<?php include 'config/connection.php'; ?>

<style type="text/css">
  input{
    border-width: 0.1px;
  }
</style>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Sales Summary</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index2.php?page=home">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="float-sm-left">
                Report Type:
                <select>
                  <option>Sales Summary by date</option>
                  <option>Sales Summery by department</option>
                </select>
                From:  <input type="date" name="fromdate" required>
                To: <input type="date" name="todate" required>
                Till No: <input type="text" name="fromtill" placeholder="01" style="width: 30px;" readonly required>
                To Till No: <input type="text" name="totill" placeholder="99" style="width: 30px;" required readonly>
              </div>
              <div class="float-sm-right">
                <button class="btn btn-sm btn-success " type="submit" name="submit">Refresh</button>
              </div>
              </div>
              <div class="card-body">
                
              </div>
              <div class="card-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
