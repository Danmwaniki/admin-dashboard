<?php require 'config/connection.php'; ?>
<style type="text/css">
    .highcharts-figure,
.highcharts-data-table table {
    min-width: 360px;
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}

.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}

.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
    padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}

.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
<figure class="highcharts-figure">
    <div id="chartjs_bar"></div>
    <p class="highcharts-description">
        Basic line chart showing trends in a dataset. This chart includes the
        <code>series-label</code> module, which adds a label to each line for
        enhanced readability.
    </p>
</figure>
</div>
</div>
</div>
</section>
</div>
<?php 
    $chartQuery = "SELECT date,sales,((COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) + sales ) as net_sales,(COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) as deposits,banking, (banking+COALESCE(non_cash,0) - (COALESCE(net_sales,0) + COALESCE(deposits,0) + COALESCE(on_acc,0) + COALESCE(caps,0))) as variance FROM eod  WHERE date::date BETWEEN '2021-10-01' AND last_day(date)  order by date ASC";

      $chartQueryRecords = pg_query($conn, $chartQuery) or die (pg_last_error($conn));
    $chart_data="";
    while ($rows = pg_fetch_array($chartQueryRecords)) { 
         
        $date[]  = $rows['date'];
        $sales[] = $rows['sales'];
        $net_sales[] = $rows['net_sales'];
        $deposits[] = $rows['deposits'];
        $banking[] = $rows['banking'];
        $variance[] = $rows['variance'];

    }

 ?>
<script type="text/javascript">
      Highcharts.chart('container', {

    title: {
        text: 'Solar Employment Growth by Sector, 2010-2016'
    },

    subtitle: {
        text: 'Source: thesolarfoundation.com'
    },

    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },

    xAxis: {
        accessibility: {
            rangeDescription: 'Range: <?php echo date("01-M-Y" ,strtotime(json_encode($date))); ?> to <?php echo date("d-M-Y" ,strtotime(json_encode($date))); ?>'
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: '<?php echo date("01-M-Y" ,strtotime(json_encode($date))); ?>';
        }
    },

    series: [{
        name: 'Sales',
        data: <?php echo json_encode($sales);?>
    }, {
        name: 'Net Sales',
        data: <?php echo json_encode($net_sales);?>
    }, {
        name: 'Deposits',
        data: <?php echo json_encode($deposits);?>
    }, {
        name: 'Banking',
        data: <?php echo json_encode($banking);?>
    }, {
        name: 'Variance',
        data: <?php echo json_encode($variance);?>
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
  </script>