<?php 
session_start();

ob_start();
// Load the database configuration file 
include_once 'config/connection.php'; 
 
$receipt_no = $_SESSION['receipt_no'];

// Filter the excel data 
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
} 
 
// Excel file name for download 
$fileName = "receipt_details_" . date('Y-m-d') . ".xls"; 
 
// Column names 
$fields = array('Recipt No', 'Date/Time', '#', 'Stockcode', 'Description', 'Qty', 'U.O.M', '@price', 'Disc(%)', 'Amount'); 
 
// Display column names as first row 
$excelData = implode("\t", array_values($fields)) . "\n"; 
 
// Fetch records from database 
$query = pg_query($conn, "SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,till_trans_details.input_date FROM till_trans_details LEFT JOIN packing on packing.pack_id = till_trans_details.uom WHERE doc_no = '$receipt_no'"); 
if($query->num_rows > 0){ 
    // Output each row of the data 
    while($row = $query->pg_fetch_assoc()){
    $i = 1; 
        $status = ($row['status'] == 1)?'Active':'Inactive'; 
        $lineData = array($row['doc_no'],$row['input_date'],$i++, $row['code'], $row['description'], $row['qty'], $row['uom'], $row['unit_incl'], $row['discount'], $row['total_incl'], $status); 
        array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
    } 
}else{ 
    $excelData .= 'No records found...'. "\n"; 
} 
 
// Headers for download 
header("Content-Type: application/vnd.ms-excel"); 
header("Content-Disposition: attachment; filename=\"$fileName\""); 
 
// Render excel data 
echo $excelData; 
 
 ob_clean();
exit;