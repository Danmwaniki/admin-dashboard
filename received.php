

  <?php if (isset($_GET['doc_no'])) {
    $id = $_GET['doc_no'];

    $qry = pg_query($conn, "SELECT * FROM st_trans WHERE doc_no = '$id'") or die(pg_last_error($conn));
    $row = pg_fetch_assoc($qry);

    $dsql = pg_query($conn, "SELECT * FROM st_trans_details WHERE doc_no = '$id'") or die(pg_last_error($conn));

  } 
  ?>
 
  <style type="text/css">
    input{
      border-style: double;
      border-color: inherit;
      border-width: 0.1rem;
      background-color: #f8f8ff;
      border-bottom-right-radius: 6px;
      border-bottom-left-radius: 6px;
      border-top-right-radius: 6px;
      border-top-left-radius: 6px;
    }
    .header{
      text-align: center;
    }
  </style>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Goods Received</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
           <div class="card shadow-lg">
            <div class="card-header">
              <div class="card-title">
              Doc No. <b><?php echo $id ?></b>  
              Date: <b><?php $date = date('d-m-Y', strtotime($row['date'])); echo $date ?></b>  
              </div>
              <div class="card-tools">
              A/C No. <input type="text" name="" id="input" style="width: 50px;" value="<?php echo $row['accno'] ?>" readonly> 
               <input type="text" name="" value="<?php echo $row['supplier_name'] ?>" readonly>
              Posted At: <input type="datetime" name="" id="input" value="<?php echo $row['input_date'] ?>" readonly>
              </div>
              </div>
            <div class="card-body table-responsive p-0">
              <table class="table table-sm table-bordered table-striped table-hover text-nowrap">
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Code</td>
                    <td>Description</td>
                    <td>Qty</td>
                    <td>Unit</td>
                    <td>Vat</td>
                    <td>Cost Price</td>
                    <td>Goods Value</td>
                    <td>VAT</td>
                    <td>Amount</td>
                  </tr>
                </thead>
                <tbody>
                    <?php 
                    $numrows = pg_num_rows($dsql);
                    for($ri = 0; $ri < $numrows; $ri++) {
                    echo "<tr>";
                    $drow = pg_fetch_assoc($dsql);
                    echo "
                     <td>", $ri, "</td>
                     <td>", $drow['stockcode'], "</td>
                     <td>", $drow['description'], "</td>
                     <td>", $drow['qty'], "</td>
                     <td>", $drow['unit'], "</td>
                     <td>", $drow['vatcode'], "</td>
                     <td>", number_format($drow['vatunitpr'],2), "</td>
                     <td>", number_format($drow['lngoods'],2), "</td>
                     <td>", number_format($drow['lnvat'],2), "</td>
                     <td>", number_format($drow['lntotal'],2), "</td>
                    </tr>";
                  }
                    ?>
                </tbody>
              </table>
            </div>
             <div class="card-footer">
              <div class="row">
                <div class="col-12">
                  Totals:
                <?php echo $numrows ?>
                  <div class=" float-sm-right">
                  <input type="text" name="" id="input" value="<?php echo number_format($row['tlngoods'],2) ?>" readonly>
                  <input type="text" name="" id="input" style="width: 60px;" value="<?php echo number_format($row['tlnvat'],2) ?>" readonly>
                  <input type="text" name="" id="input" value="<?php echo number_format($row['tlntotal'],2) ?>" readonly>
                </div>
              </div>
              </div>
              <br>
              <div class="row">
                <div class="col-12">
              <button type="button" data-toggle="modal" data-target="#receivedModal" class="btn btn-default"><i class="fas fa-print"></i>Print</button>
              <div class="float-sm-right">
                <button class="btn btn-sm btn-success"><i class="fas fa-file-export"></i> Export</button>
                <button class="btn btn-sm btn-danger"><i class="fa fa-file-pdf" aria-hidden="true"></i>  Pdf</button>
              </div>
            </div>
            </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

       <div class="modal fade" id="receivedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog modal-lg shadow-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="row">
                  <div class="col-sm-12">
                    <h2 style="text-align: center;" id="exampleModalLabel">New message</h2>
                  </div>
                </div>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <label for="recipient-name" class="control-label">Recipient:</label>
                    <input type="text" class="form-control" id="recipient-name">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="control-label">Message:</label>
                    <textarea class="form-control" id="message-text"></textarea>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
              </div>
            </div>
          </div>
        </div>

  <script type="text/javascript"> 
  
    // Create a div element
const fakeEle = document.createElement('div');

// Hide it completely
fakeEle.style.position = 'absolute';
fakeEle.style.top = '0';
fakeEle.style.left = '1px';
fakeEle.style.overflow = 'hidden';
fakeEle.style.visibility = 'hidden';
fakeEle.style.whiteSpace = 'nowrap';
fakeEle.style.height = 'auto';
fakeEle.style.width = 'autoFill';

// We copy some styles from the textbox that effect the width
const textboxEle = document.getElementById('input');

// Get the styles
const styles = window.getComputedStyle(textboxEle);

// Copy font styles from the textbox
fakeEle.style.fontFamily = styles.fontFamily;
fakeEle.style.fontSize = styles.fontSize;
fakeEle.style.fontStyle = styles.fontStyle;
fakeEle.style.fontWeight = styles.fontWeight;
fakeEle.style.letterSpacing = styles.letterSpacing;
fakeEle.style.textTransform = styles.textTransform;

fakeEle.style.borderLeftWidth = styles.borderLeftWidth;
fakeEle.style.borderRightWidth = styles.borderRightWidth;
fakeEle.style.paddingLeft = styles.paddingLeft;
fakeEle.style.paddingRight = styles.paddingRight;

// Append the fake element to `body`
document.body.appendChild(fakeEle);
const setWidth = function () {
    const string = textboxEle.value || textboxEle.getAttribute('placeholder') || '';
    fakeEle.innerHTML = string.replace(/\s/g, '&' + 'nbsp;');

    const fakeEleStyles = window.getComputedStyle(fakeEle);
    textboxEle.style.width = fakeEleStyles.width;
};
setWidth();

textboxEle.addEventListener('input', function (e) {
    setWidth();
});
  </script>