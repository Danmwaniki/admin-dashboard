

<div class="content-wrapper">
	<div class="content-header">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Goods Receivables</h1>
			</div>
			<!-- /.div header -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
					<li class="breadcrumb-item active"><?php echo $page ?></li>
				</ol>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.content-header -->

	<!-- Main Content -->
	<section class="content">
		<div class="content-fluid">
			<div class="row">
				<div class="col-12">
					<div id="card" class="card shadow-lg">
						<form method="post">
						<div class="card-header">
							<div class="float-sm-left">
								<?php 
									$trans_qry = pg_query($conn, "SELECT * FROM st_type WHERE sub_type = 'REC'") or die(pg_last_error($conn));

								 ?>
							Transaction Type:
							<select name="type" id="type" required autofocus>
								<option class="active" value="REC">All</option>
								<?php 
								while ($trans_assoc = pg_fetch_assoc($trans_qry)) {
								?>
								<option value="<?php echo $trans_assoc['type'] ?>"><?php echo $trans_assoc['long_desc'] ?></option>
								<?php
								}
								?>
							</select>
							</div>
							<div class="card-tools">
								From:
								<input type="date" name="fromDate" autofocus required>
								To:
								<input type="date" name="toDate" required>
								<button type="submit" class="btn btn-sm btn-primary shadow-lg" name="refresh">Refresh</button>
							</div>
						</div>
					</form>
						<div class="card-body table-responsive p-0">
							<table id="example2" class="table table-sm table-bordered table-striped table-hover text-nowrap">
								<thead>
									<tr>
										<td>Date</td>
										<td>Doc No.</td>
										<td>Type</td>
										<td>A/C</td>
										<td>Name</td>
										<td>Qty</td>
										<td>Goods Value</td>
										<td>VAT Value</td>
										<td>Total</td>
										<td>Input BY</td>
									</tr>
								</thead>
								<tbody>
									<?php 
										if (isset($_POST['refresh'])) {
											extract($_POST);

											if ($type == "REC") {
											$a_qry = pg_query($conn, "SELECT date, doc_no, st_trans.type, accno, name, tqty, tlngoods, tlnvat, tlntotal, input_by FROM st_trans JOIN st_type ON st_trans.type = st_type.type WHERE st_type.sub_type = 'REC' AND date BETWEEN '$fromDate' and '$toDate' order by date ASC") or die(pg_last_error($conn));
											while ($row_2 = pg_fetch_array($a_qry)) {
																									
												?>
												<tr>
													<td><?php echo date('d-m-Y', strtotime($row_2['date'])) ?></td>
													<td><button type="button" id="docno" class="btn btn-sm btn-outline-primary" onclick="myFunction()" value="<?php echo $row_2['doc_no'] ?>"><?php echo $row_2['doc_no'] ?></button></td>
													<td><?php echo $row_2['type'] ?></td>
													<td><?php echo $row_2['accno'] ?></td>
													<td><?php echo $row_2['name'] ?></td>
													<td><?php echo $row_2['tqty'] ?></td>
													<td><?php echo number_format($row_2['tlngoods'],2) ?></td>
													<td><?php echo number_format($row_2['tlnvat'],2) ?></td>
													<td><?php echo number_format($row_2['tlntotal'],2) ?></td>
													<td><?php echo $row_2['input_by'] ?></td>
												</tr>
											<?php

										}

											}else{

										$qry = pg_query($conn, "SELECT date, doc_no, st_trans.type, accno, name, tqty, tlngoods, tlnvat, tlntotal, input_by FROM st_trans JOIN st_type ON st_trans.type = st_type.type WHERE st_trans.type = '$type' AND date BETWEEN '$fromDate' and '$toDate' order by date ASC") or die(pg_last_error($conn));
											while ($row_1 = pg_fetch_array($qry)) {
																									
												?>
												<tr>
													<td><?php echo date('d-m-Y', strtotime($row_1['date'])) ?></td>
													<td><button type="button" id="docno" class="btn btn-sm btn-outline-primary" onclick="myFunction()" value="<?php echo $row_1['doc_no'] ?>"><?php echo $row_1['doc_no'] ?></button></td>
													<td><?php echo $row_1['type'] ?></td>
													<td><?php echo $row_1['accno'] ?></td>
													<td><?php echo $row_1['name'] ?></td>
													<td><?php echo $row_1['tqty'] ?></td>
													<td><?php echo number_format($row_1['tlngoods'],2) ?></td>
													<td><?php echo number_format($row_1['tlnvat'],2) ?></td>
													<td><?php echo number_format($row_1['tlntotal'],2) ?></td>
													<td><?php echo $row_1['input_by'] ?></td>
												</tr>
											<?php
										}
											$rows = pg_num_rows($qry);
										}
										}

									?>
								</tbody>
							</table>
						</div>
						<div class="card-footer">
							<div class="float-sm-left">
								Totals:
							</div>
							<div class="float-sm-right">
								
								<?php 
								if (isset($_POST['refresh'])) {

									if ($type == "REC") {
									$t_qry = pg_query($conn, "SELECT sum(a.tqty) as qty, sum(a.tlngoods) as goods, sum(a.tlnvat) as vat, sum(a.tlntotal) as total FROM(SELECT date, doc_no, st_trans.type, accno, name, tqty, tlngoods, tlnvat, tlntotal, input_by FROM st_trans JOIN st_type ON st_trans.type = st_type.type WHERE st_type.sub_type = 'REC' AND date BETWEEN '$fromDate' and '$toDate') as a") or die(pg_last_error($conn));

										while($t_rows = pg_fetch_assoc($t_qry)){

										?>

								Qty's:
								<input type="text" name="total_qtys" value="<?php echo $t_rows['qty']?>" style="width: 4rem; border-color: inherit; border-width: 0.1rem;" readonly>
								Good's Value:
								<input type="text" name="goods" value="<?php echo number_format($t_rows['goods'],2) ?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem;" readonly>
								VAT: 
								<input type="text" name="vat" value="<?php echo number_format($t_rows['vat'],2)?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem;" readonly>
								Total:
								<input type="text" name="total" value="<?php echo number_format($t_rows['total'],2) ?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem;" readonly>

								<?php 
							}
						}elseif($type){

									$goods = pg_query($conn, "SELECT sum(a.tqty) as qty, sum(a.tlngoods) as goods, sum(a.tlnvat) as vat, sum(a.tlntotal) as total FROM (SELECT date, doc_no, st_trans.type, accno, name, tqty, tlngoods, tlnvat, tlntotal, input_by FROM st_trans JOIN st_type ON st_trans.type = st_type.type WHERE st_trans.type = '$type' AND date BETWEEN '$fromDate' and '$toDate') as a") or die(pg_last_error($conn));
									while($g_rows = pg_fetch_assoc($goods)){

										?>

								Qty's:
								<input type="text" name="total_qtys" value="<?php echo $g_rows['qty']?>" style="width: 4rem; border-color: inherit; border-width: 0.1rem;" readonly>
								Good's Value:
								<input type="text" name="goods" value="<?php echo number_format($g_rows['goods'],2) ?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem;" readonly>
								VAT: 
								<input type="text" name="vat" value="<?php echo number_format($g_rows['vat'],2)?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem;" readonly>
								Total:
								<input type="text" name="total" value="<?php echo number_format($g_rows['total'],2) ?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem;" readonly>

							
							<?php
								}
								}else{

								?>
								Qty's:
								<input type="text" name="total_qtys" value="<?php echo number_format(0,2) ?>" style="width: 4rem; border-color: inherit; border-width: 0.1rem; text-align: right;" readonly>
								Good's Value:
								<input type="text" name="goods" value="<?php echo number_format(0,2) ?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem; text-align: right;" readonly>
								VAT: 
								<input type="text" name="vat" value="<?php echo number_format(0,2)?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem; text-align: right;" readonly>
								Total:
								<input type="text" name="total" value="<?php echo number_format(0,2) ?>" style="width: 6rem; border-color: inherit; border-width: 0.1rem; text-align: right;" readonly>

								<?php

									}
								}

								?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.main content -->
</div>
