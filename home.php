
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
                       <?php 
                $query="SELECT COALESCE(sum(data.amount),0) as bal_bf from (SELECT a.sub_type,sum(a.amount ) as amount FROM(SELECT sub_type,
                        CASE 
                          WHEN st_type.sign = '+' THEN
                            sum(st_trans_details.lncost)
                          ELSE
                            sum(st_trans_details.lncost)*-1
                        END as amount
                         FROM st_type 
                         JOIN
                         st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period < (SELECT sys_trans_master.period FROM sys_trans_master WHERE sys_trans_master.type = 'ST')
                          GROUP BY sub_type,st_type.sign ORDER BY sub_type) a GROUP BY sub_type)as data";
                      $result = pg_query($conn, $query) or die (pg_last_error($conn));
                      $row=pg_fetch_assoc($result);
                      $bal_bf = $row['bal_bf'];

                $query="SELECT COALESCE(sum(data.current_amnt),0) as cltotal from 
                        (SELECT a.sub_type,a.group_desc,COALESCE(sum(a.amount ),0) as current_amnt FROM(SELECT sub_type,group_desc,
                    COALESCE(CASE 
                      WHEN st_type.sign = '+' THEN
                        sum(st_trans_details.lncost)
                      ELSE
                        sum(st_trans_details.lncost)*-1
                    END,0) as amount
                     FROM st_type 
                     JOIN
                     st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period = (SELECT sys_trans_master.period FROM sys_trans_master WHERE sys_trans_master.type = 'ST')
                      GROUP BY sub_type,group_desc,st_type.sign ORDER BY sub_type) a GROUP BY sub_type,group_desc ORDER BY current_amnt ASC) as data";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                    $row=pg_fetch_assoc($result);
                    $current = $row['cltotal'];

                     $query="SELECT COALESCE(sum(adv.advance),0) as sales FROM (SELECT a.sub_type,st_type.group_desc,sum(a.amount) as advance FROM (SELECT sub_type,st_type.sign,
                      CASE 
                      WHEN st_type.sign = '+' THEN
                         sum(st_trans_details.lncost)
                      ELSE
                        sum(st_trans_details.lncost)*-1
                      END as amount
                      FROM  st_type
                      JOIN st_trans_details ON st_type.type = st_trans_details.type 
                      WHERE st_trans_details.period > (SELECT period FROM sys_trans_master WHERE sys_trans_master.type = 'ST') group by sub_type,st_type.sign order by sub_type) as a join st_type on a.sub_type=st_type.sub_type group by a.sub_type,st_type.group_desc)as adv";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                    $row=pg_fetch_assoc($result);
                    $advanced = $row['sales'];


                    $sales = $bal_bf + $current;

                    $actual = $sales + $advanced;

                 ?>
                 <!-- Info boxes -->
            <div class="row">
              <div class="col-md-4">
                <!-- first card -->
                <div class="card card-widget widget-user direct-chat small-box bg-info shadow-lg">
                  <div class="card-header">
                    <h3 class="card-title">Inventory</h3>
                    <div class="card-tools">
                      <span class="badge"><i style="font-size: medium; color: inherit;"><?php echo number_format($actual,2) ?></i></span>
                      <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="index.php?page=home" data-source-selector="#card-refresh-content" data-load-on-init="false">
                    <i class="fas fa-sync-alt"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- ./card-header -->
                  <div class="card-body" id="accordion">
              
                       <?php
                $query="SELECT period FROM sys_trans_master WHERE type='ST'";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                    $st_r=pg_fetch_assoc($result);
                    $_SESSION['period'] = $st_r['period'] ??= 'default value';

                    $period = $_SESSION['period'];
                 ?>
                      
                   <div class="direct-chat-messages" style="height: auto;">     
                  <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-left" style="font-weight: lighter;">Current Period:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: black;"><?php $period = date("d-m-Y", strtotime($st_r['period'] ??= 'default value')); 
                   echo $period; ?></i></span>
                  </div>
                  <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-left" style="font-weight: lighter;">Bal B/F:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: black;"><?php  echo number_format($bal_bf,2); ?></i></span>
                  </div>
                  <div class="direct-chat-infos clearfix">
                    <a class="d-block w-100" style="color: inherit;" data-toggle="collapse" href="#currentStock">
                    <span class="direct-chat-name float-left" style="font-weight: lighter;">Current Stock:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: black;"><?php  echo number_format($sales,2); ?></i></span>
                    </a>
                  </div>
                  <!-- current staock -->
                   <?php 
                     $c_qry = pg_query($conn, "SELECT a.sub_type,a.group_desc,COALESCE(sum(a.amount ),0) as current_amnt FROM(SELECT sub_type,group_desc,
                        COALESCE(CASE 
                          WHEN st_type.sign = '+' THEN
                            sum(st_trans_details.lncost)
                          ELSE
                            sum(st_trans_details.lncost)*-1
                        END,0) as amount
                         FROM st_type 
                         JOIN
                         st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period = (SELECT sys_trans_master.period FROM sys_trans_master WHERE sys_trans_master.type = 'ST')
                          GROUP BY sub_type,group_desc,st_type.sign ORDER BY sub_type) a GROUP BY sub_type,group_desc ORDER BY current_amnt ASC") or die (pg_last_error($conn));
                      while ($c_rw = pg_fetch_array($c_qry)){
                        ?>
                    <div id="currentStock" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left"><i style="font-size: x-small;"><?php echo" ",$c_rw['sub_type'] ??= 'Trans Type'," - ",$c_rw['group_desc'] ??= 'Desription'," "?></i></span>
                      <span class="direct-chat-timestamp float-right"><i style="color: black;"><?php echo  number_format($c_rw['current_amnt'],2) ?></i></span>
                  </div>
                <?php } ?>
                  <!-- /.advance details -->
                  <div class="direct-chat-infos clearfix">
                    <a class="d-block w-100" style="color: inherit;" data-toggle="collapse" href="#advanceStock">
                    <span class="direct-chat-name float-left" style="font-weight: lighter;">Advanced Stock:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: black;"><?php  echo number_format($advanced,2); ?></i></span>
                    </a>
                  </div>
                  <!-- advance details -->
                     <?php 
                $query="SELECT a.sub_type,st_type.group_desc,sum(a.amount) as advance FROM (SELECT sub_type,st_type.sign,
                      CASE 
                      WHEN st_type.sign = '+' THEN
                         sum(st_trans_details.lncost)
                      ELSE
                        sum(st_trans_details.lncost)*-1
                      END as amount
                      FROM  st_type
                      JOIN st_trans_details ON st_type.type = st_trans_details.type 
                      WHERE st_trans_details.period > (SELECT period FROM sys_trans_master WHERE sys_trans_master.type = 'ST') group by sub_type,st_type.sign order by sub_type) as a join st_type on a.sub_type=st_type.sub_type group by a.sub_type,st_type.group_desc";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                  while ($row = pg_fetch_array($result)){
                    ?>
                    <div id="advanceStock" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left"><i style="font-size: x-small;"><?php echo" ",$row['sub_type'] ??= 'Trans Type'," - ",$row['group_desc'] ??= 'Desription'," "?></i></span>
                      <span class="direct-chat-timestamp float-right"><i style="color: black;"><?php echo  number_format($row['advance'],2) ?></i></span>
                  </div>
                <?php } ?>
                  <!-- /.advance details -->
                </div>
                  <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
                  </div>
                  <a href="inventory/index2.php?page=home" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>

              <!-- /.frist card -->

              <!-- second card -->
                <div class="col-md-4">
                 <?php
                    $f_qry = pg_query($conn, "SELECT max(date) as c_date, date_trunc('month',max(date)) as f_date FROM eod;") or die(pg_last_error($conn));
                    $f_row = pg_fetch_assoc($f_qry);
                    $f_day = $f_row['f_date'];
                    $c_day = $f_row['c_date'];


                    $s_query = pg_query($conn, "SELECT sum(a.customers) as total_customers, sum(a.sales) as total_sales, sum(a.net_sale) as net_sales, sum(a.deposits) as total_deposits, sum(a.pickups) as total_pickups, sum(a.cheque) as total_cheque, sum(a.credit_cards) as total_credit, sum(a.m_pesa) as total_pesa, sum(a.banking) as total_banking, sum(a.variance) as total_variance from (SELECT date,customers,sales,expenses,((COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) + sales ) as net_sale,(COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) as deposits,(pickups+COALESCE(cash_balance,0)) as pickups,credit_cards,m_pesa,banking, (banking+COALESCE(non_cash,0) - (COALESCE(net_sales,0) + COALESCE(deposits,0) + COALESCE(on_acc,0) + COALESCE(caps,0))) as variance,COALESCE(non_cash,0)  as cash_balance,cheque FROM eod  WHERE date::date BETWEEN '$f_day' AND '$c_day'  order by date ASC) as a") or die(pg_last_error($conn));
                    $s_row = pg_fetch_assoc($s_query);

                    


                    $v_qry = pg_query($conn, "SELECT sum(COALESCE(non_cash,0)) as non_cash,sum(COALESCE(on_acc,0)) as on_acc,sum(COALESCE(caps,0)) as caps FROM eod WHERE date::date BETWEEN '$f_day' AND '$c_day'") or die(pg_last_error($conn));
                    $v_row = pg_fetch_assoc($v_qry);

                    ?>
                <!-- first card -->
                <div class="card card-widget widget-user direct-chat small-box bg-success shadow-lg">
                  <div class="card-header">
                    <h3 class="card-title" style="color: black; font-size: medium;font-family: Times New Roman;">Sales</h3>
                    <div class="card-tools">
                      <span class="badge "><i style="font-size: medium; color: black;"><?php  echo number_format($s_row['total_sales'],2); ?></i></span>
                      <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="index.php?page=home" data-source-selector="#card-refresh-content" data-load-on-init="false" style="color: black;">
                    <i class="fas fa-sync-alt"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse" style="color: black;">
                        <i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- ./card-header -->
                  <div class="card-body" id="accordion">
                   <div class="direct-chat-messages" style="height: auto;">     
                  <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Current Month:</span>
                    <?php 
                      $pos_qry = pg_query($conn, "SELECT MAX(period) as period from pos_pay_trans") or die(pg_last_error($conn));
                      $pos_r = pg_fetch_assoc($pos_qry);
                     ?>
                      <span class="direct-chat-timestamp float-right" style="color: inherit;font-weight: lighter;"><i><?php $period = date("d-m-Y", strtotime($pos_r['period'] ??= 'default value')); 
                   echo $period; ?></i></span>
                  </div>
                  <div class="direct-chat-infos clearfix">
                    <a class="d-block w-100" data-toggle="collapse" href="#banking">
                    <span class="direct-chat-name float-left" style="color: inherit; color: black; font-weight: lighter;">Banking:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: black;"><?php  echo number_format($s_row['total_banking'],2); ?></i></span>
                    </a>
                  </div>

                  <div id="banking" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Mobile Money:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_pesa'],2); ?></i></span>
                  </div>
                   <div id="banking" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Credit Card:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_credit'],2); ?></i></span>
                  </div>
                   <div id="banking" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Cheques Picked:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_cheque'],2); ?></i></span>
                  </div>
                    <div id="banking" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Total Cash Picked:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_pickups'],2); ?></i></span>
                  </div>

                  <div class="direct-chat-infos clearfix">
                    <a class="d-block w-100" data-toggle="collapse" href="#netSales">
                    <span class="direct-chat-name float-left" style="color: inherit; color: black; font-weight: lighter;">Net Sales:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: black;"><?php  echo number_format($s_row['net_sales'],2); ?></i></span>
                    </a>
                  </div>

                  <div id="netSales" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Collections:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_deposits'],2); ?></i></span>
                  </div>

                  <div id="netSales" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Less Total Sales:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_sales'],2); ?></i></span>
                  </div>
                  <!-- /.variance details -->
                  <div class="direct-chat-infos clearfix">
                    <a class="d-block w-100" style="color: inherit;" data-toggle="collapse" href="#variance">
                    <span class="direct-chat-name float-left" style="color: inherit; color: black; font-weight: lighter;">Variance:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: black;"><?php  echo number_format($s_row['total_variance'],2); ?></i></span>
                    </a>
                  </div>
                  <div id="variance" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Banking:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_banking'],2); ?></i></span>
                  </div>
                  <div id="variance" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Non Cash:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php echo number_format($v_row['non_cash'],2) ?></i></span>
                  </div>
                  <div id="variance" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Less Net Sales:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['net_sales'],2); ?></i></span>
                  </div>
                  <div id="variance" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Less Deposits:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($s_row['total_deposits'],2); ?></i></span>
                  </div>
                  <div id="variance" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Less On account:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($v_row['on_acc'],2); ?></i></span>
                  </div>
                   <div id="variance" class="collapse direct-chat-infos clearfix" data-parent="#accordion">
                    <span class="direct-chat-name float-left" style="color: inherit; font-weight: lighter;">Less Caps Payments:</span>
                      <span class="direct-chat-timestamp float-right"><i  style="color: whitesmoke;"><?php  echo number_format($v_row['caps'],2); ?></i></span>
                  </div>
                </div>
                  <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
                  </div>
                  <a href="index.php?page=sales_summary" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>

              <!-- /.frist card -->

              <!-- /.second card -->
        </div>
        <!-- /.row -->

         
                    <?php  

                    $f_qry = pg_query($conn, "SELECT max(date) as c_date, date_trunc('month',max(date)) as f_date FROM eod;") or die(pg_last_error($conn));
                    $f_row = pg_fetch_assoc($f_qry);
                    $f_day = $f_row['f_date'];
                    $c_day = $f_row['c_date'];

                      $handle = pg_query($conn, "SELECT date,customers,sales,expenses,((COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) + sales ) as net_sales,(COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) as deposits,(pickups+COALESCE(cash_balance,0)) as pickups,credit_cards,m_pesa,banking, (banking+COALESCE(non_cash,0) - (COALESCE(net_sales,0) + COALESCE(deposits,0) + COALESCE(on_acc,0) + COALESCE(caps,0))) as variance,COALESCE(non_cash,0)  as cash_balance,cheque FROM eod  WHERE date::date BETWEEN '$f_day' AND '$c_day'  order by date ASC") or die(pg_last_error($conn));

                    
                      $chart_data = "";
                      while($result = pg_fetch_array($handle)){

                      $chart_date[] = date('d-m-Y', strtotime($result['date'] ??= 'default value')) ;
                      $chart_sales[] = $result['sales'];
                      $chart_banking[] = $result['banking'];
                      $chart_expenses[] = $result['expenses'];
                      $chart_variance[] = $result['variance'];
                    }

                   $pie_query = pg_query($conn, "SELECT sum(a.customers) as total_customers, sum(a.sales) as total_sales, sum(a.net_sale) as net_sales, sum(a.deposits) as total_deposits, sum(a.pickups) as total_pickups, sum(a.cheque) as total_cheque, sum(a.credit_cards) as total_credit, sum(a.m_pesa) as total_pesa, sum(a.banking) as total_banking, sum(a.variance) as total_variance from (SELECT date,customers,sales,expenses,((COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) + sales ) as net_sale,(COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) as deposits,(pickups+COALESCE(cash_balance,0)) as pickups,credit_cards,m_pesa,banking, (banking+COALESCE(non_cash,0) - (COALESCE(net_sales,0) + COALESCE(deposits,0) + COALESCE(on_acc,0) + COALESCE(caps,0))) as variance,COALESCE(non_cash,0)  as cash_balance,cheque FROM eod  WHERE date::date BETWEEN '$f_day' AND '$c_day'  order by date ASC) as a") or die(pg_last_error($conn));

                    
                    $pieResult = pg_fetch_assoc($pie_query);

                      $pie_customers = $pieResult['total_customers'];
                      $pie_sales = $pieResult['total_sales'];
                      $pie_deposits = $pieResult['total_deposits'];
                      $pie_pickups = $pieResult['total_pickups'];
                      $pie_cheques = $pieResult['total_cheque'];
                      $pie_credit = $pieResult['total_credit'];
                      $pie_pesa = $pieResult['total_pesa'];
                      $pie_banking = $pieResult['total_banking'];
                      $pie_variance = $pieResult['total_variance'];
                    

                      pg_close($conn);
                  ?>
           <div class="row">
          <div class="col-md-6">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card shadow-lg">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Sales
                </h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Sales</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#summary-chart" data-toggle="tab">Summary</a>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;">
                  <canvas id="barChart" height="300" style="height: 300px;"></canvas>
                </div>
                  <div class="chart tab-pane" id="summary-chart" style="position: relative; height: 300px;">
                    <canvas id="pieChart" height="300" style="height: 300px;"></canvas>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col (RIGHT) -->
        </div>
        <!-- /.row -->
      <!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>  

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->
<!-- Page specific script -->
  <script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var barChartCanvas = $('#barChart').get(0).getContext('2d')

    var barChartData = {
      labels  : <?php echo json_encode($chart_date);?>,
      datasets: [
        {
          label               : 'Total Sales',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : <?php echo json_encode($chart_sales); ?>
        },
        {
          label               : 'Banking',
          backgroundColor     : 'rgba(46, 204, 113, 1)',
          borderColor         : 'rgba(210, 214, 222, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : <?php echo json_encode($chart_banking); ?>
        },
         {
          label               : 'Variance',
          backgroundColor     : 'rgba(192, 57, 43, 1)',
          borderColor         : 'rgba(192, 57, 43, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : <?php echo json_encode($chart_variance); ?>
        },
      ]
    }

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    // This will get the first returned node in the jQuery collection.
   new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

  

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = {
      labels: [
          'Customers',
          'Sales',
          'Deposits',
          'Pickups',
          'Cheques',
          'Credit Cards',
          'Mpesa',
          'Banking',
          'Varinace',
      ],
      datasets: [
        {
          data: [<?php echo $pie_customers ?>,
          <?php echo $pie_sales ?>,
          <?php echo $pie_deposits ?>,
          <?php echo $pie_pickups ?>,
          <?php echo $pie_cheques ?>,
          <?php echo $pie_credit ?>,
          <?php echo $pie_pesa ?>,
          <?php echo $pie_banking ?>,
          <?php echo $pie_variance ?>],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#bdb76b', '#f19cbb','#2e2e00', '#ad7100', '#ff0000'],
        }
      ]
    }
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    }) 
  
})
</script>