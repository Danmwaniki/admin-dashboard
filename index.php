<!DOCTYPE html>
<html lang="en">

<?php session_start();
require ('config/connection.php'); ?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title><?php echo isset($_SESSION['company']['name']) ? $_SESSION['company']['name'] : '' ?></title>


<?php
   if(!isset($_SESSION['login_id'])){
    echo "<script type='text/javascript'>alert('Error: Logins Needed')</script>";
    header('location:login.php');
   };

   require './header.php';
   ?>
</head>


<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed text-sm">
<div class="wrapper">

  <?php include 'navbar.php'; ?>
  <?php include 'sidebar.php'; ?>


 <main>
      <?php $page = isset($_GET['page']) ? $_GET['page'] :'home'; ?>
    <?php include $page.'.php' ?>
  </main>  

 <!-- Preloader 
    <div class="preloader flex-column justify-content-center align-items-center" style="opacity: 50%;">
    <img class="animation__wobble" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
    <div id="preloader"></div> -->
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
  </div> 
  <!-- preloader -->

  <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>

      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Account Details</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <label for="userName">Username:</label>
              <div class="input-group mb-3">
                <input type="text" name="username" class="form-control" id="userName" value="<?php echo $_SESSION['login_username'] ?>" readonly>
                 <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
              </div>
              <label for="name">Name:</label>
              <div class="input-group mb-3">
                <input type="text" name="username" class="form-control" id="name" value="<?php echo $_SESSION['login_name'] ?>" readonly>
                 <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
              </div>
              <label for="passWord">Password</label>
              <div class="input-group mb-3">
                <input type="password" name="password" class="form-control" id="passWord" value="<?php echo $_SESSION['login_password'] ?>" readonly>
                 <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Company Details</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <?php
              $co_qry = pg_query($conn, "SELECT company.name as c_name, branch.name as b_name,address,phone,pin_no FROM company LEFT JOIN branch ON company.branch = branch.code") or die(pg_last_error($conn));
              $co_rw = pg_fetch_assoc($co_qry);
               ?>
              <div class="row">
                <div class="col-5">
                   <label for="Name">Company Name:</label>
                    <div class="input-group mb-3">
                      <input type="text" name="company" class="form-control" id="Name" value="<?php echo $co_rw['c_name'] ?>" placeholder="Company Name" readonly>
                       <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="far fa-building"></span>
                      </div>
                    </div>
                    </div>
                </div>
                 <div class="col-4">
                   <label for="address">Address:</label>
                    <div class="input-group mb-3">
                      <input type="text" name="address" class="form-control" id="address" value="<?php echo $co_rw['address'] ?>" placeholder="Address" readonly>
                       <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fa fa-address-card" aria-hidden="true"></span>
                      </div>
                    </div>
                    </div>
                </div>
                 <div class="col-3">
                   <label for="phoneNumber">Phone Number:</label>
                    <div class="input-group mb-3">
                      <input type="text" name="phone" class="form-control" id="phoneNumber" value="<?php echo $co_rw['phone'] ?>" placeholder="Phone Number" readonly>
                       <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fa fa-phone" aria-hidden="true"></span>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-5">
                  <label for="pinNo">PIN NUMBER:</label>
                    <div class="input-group mb-3">
                      <input type="text" name="pin" class="form-control" id="pinNo" value="<?php echo $co_rw['pin_no'] ?>" placeholder="Pin Number" readonly>
                       <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fa fa-key" aria-hidden="true"></span>
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="col-5">
                  <label for="branch">Branch:</label>
                    <div class="input-group mb-3">
                      <input type="text" name="branch" class="form-control" id="branch" value="<?php echo $co_rw['b_name'] ?>" placeholder="Branch Name" readonly>
                       <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fa fa-building" aria-hidden="true"></span>
                      </div>
                    </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
       <div class="modal fade" id="modal-default2">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Connection Settings</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-5">
                  <label for="localServer">Local Server</label>
                  <div class="input-group mb-3">
                      <input type="text" name="port" class="form-control" id="localServer" value="" placeholder="Database Address" readonly autofocus>
                       <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fa fa-database fa-spin" aria-hidden="true"></span>
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="col-5">
                      <label for="port">Port</label>
                       <div class="input-group mb-3">
                        <input type="text" name="port" class="form-control" id="port" value="" placeholder="port" readonly autofocus>
                         <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-charging-station fa-spin" aria-hidden="true"></span>
                        </div>
                      </div>
                      </div>
                  </div>
              </div>
                 <div class="row">
                <div class="col-5">
                  <label for="User">User:</label>
                  <div class="input-group mb-3">
                      <input type="text" name="user" class="form-control" id="User" value="" placeholder="User" readonly autofocus>
                       <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fa fa-user fa-spin" aria-hidden="true"></span>
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="col-5">
                      <label for="password">Password:</label>
                       <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" id="Password" value="" placeholder="Password" readonly autofocus>
                         <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-lock fa-spin" aria-hidden="true"></span>
                        </div>
                      </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

       


  <?php include './footer.php'; ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
<?php require './scripts.php'; ?>

<script type = "text/javascript">

function myFunction(){
  var name = document.getElementById("type").value;
  var id = document.getElementById("docno").value;
  var url = "index.php?page=received" + "&doc_no=" + encodeURIComponent(id);
  window.location.href = url;

}
  
  $(document).ready(function() {
    $('#table').DataTable();
    $('#table1').DataTable();
  });
  
   $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,"searching":false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

 
</script>
</body>
</html>
