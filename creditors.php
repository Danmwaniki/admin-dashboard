<?php include 'config/conn.php'; ?>

<style type="text/css">
  input{
    border-style: groove;
    border-width: 1px;
    border-color: lightgrey;
  }
  button{
    border-style: initial;
    font-display: auto;
    font-size: large;
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
    border-bottom-right-radius: 2px;
    border-bottom-left-radius: 2px;
    background-color: dimgrey;
    color: whitesmoke;
    font-style: times new romans;
  }
  button hover{
    color: black;
    background-color: chocolate;
  }
  .new{
    background-color: lightgreen;
  }
  td:nth-child(4)
{
  background-color: lightgreen;
}
</style>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Creditors Listing</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item active">Creditors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <form method="post" class="card">
              <div class="card-header">
                <div class="float-sm-left">
                PERIOD:  <input type="text" name="period" required="" value="<?php  $query="SELECT period from sys_trans_master where type='PL';";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                    $row = pg_fetch_assoc($result); 
                    $newDate = date("Y-m-d", strtotime($row['period'])); echo($newDate) ?>"  >
              </div>
              <select name="balance">
                <option value="all">All</option>
                <option value="withbal">With Balance</option>
                <option value="withoutbal">Without Balance</option>
              </select>
                <button class="float-sm-right" type="submit" name="submit">Refresh</button>
              </div>  
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
              <thead>
              <tr>
                <th>Acc/No</th>
                <th>Name</th>
                <th>Pin No.</th>
                <th>Acc Bal</th>
                <th>Email</th>
                <th>Phone</th>
                <th>C/Person</th>
              </tr>
            </thead>
						<tbody>
              

      <?php 
      if(isset($_POST["submit"])){

        $balance  = $_POST['balance'];

        if ($balance=='all') {
          $query = "SELECT accno,name,pin,actual_balance,email, phone, contact FROM supplier GROUP BY accno,name,pin,actual_balance,email,phone,contact  ORDER BY accno ASC;";
      $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
      $numrows = pg_num_rows($result);
      for($ri = 0; $ri < $numrows; $ri++) {
        echo "<tr>";
        $row = pg_fetch_assoc($result, $ri);
          echo "<td>",$row['accno'], "</td>
        <td>", $row['name'], "</td>
        <td>", $row['pin'], "</td>
        <td>", $row['actual_balance'], "</td>
        <td>", $row['email'], "</td>
        <td>", $row['phone'], "</td>
        <td>", $row['contact'], "</td>
      </tr>";
    }

    $query ="SELECT sum(DISTINCT actual_balance) as total FROM supplier";
    $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
    $rows = pg_fetch_assoc($result);

        }elseif ($balance=='withbal') {
              $query = "SELECT accno,name,pin,actual_balance,email, phone, contact FROM supplier WHERE actual_balance >=1 GROUP BY accno,name,pin,actual_balance,email,phone,contact  ORDER BY accno ASC;";
      $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
      $numrows = pg_num_rows($result);
      for($ri = 0; $ri < $numrows; $ri++) {
        echo "<tr>";
        $row = pg_fetch_assoc($result, $ri);
          echo "<td>",$row['accno'], "</td>
        <td>", $row['name'], "</td>
        <td>", $row['pin'], "</td>
        <td>", $row['actual_balance'], "</td>
        <td>", $row['email'], "</td>
        <td>", $row['phone'], "</td>
        <td>", $row['contact'], "</td>
      </tr>";
    }

    $query ="SELECT sum(DISTINCT actual_balance) as total FROM supplier WHERE actual_balance >=1";
    $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
    $rows = pg_fetch_assoc($result);

        }elseif ($balance=='withoutbal') {
              $query = "SELECT accno,name,pin,actual_balance,email, phone, contact FROM supplier WHERE actual_balance <=0 GROUP BY accno,name,pin,actual_balance,email,phone,contact  ORDER BY accno ASC;";
      $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
      $numrows = pg_num_rows($result);
      for($ri = 0; $ri < $numrows; $ri++) {
        echo "<tr>";
        $row = pg_fetch_assoc($result, $ri);
          echo "<td>",$row['accno'], "</td>
        <td>", $row['name'], "</td>
        <td>", $row['pin'], "</td>
        <td>", $row['actual_balance'], "</td>
        <td>", $row['email'], "</td>
        <td>", $row['phone'], "</td>
        <td>", $row['contact'], "</td>
      </tr>";
    }

    $query ="SELECT sum(actual_balance) as total FROM supplier WHERE actual_balance <=0";
    $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
    $rows = pg_fetch_assoc($result);

        }


      
  }else{
     $query = "SELECT accno,name,pin,actual_balance,email, phone, contact FROM supplier GROUP BY accno,name,pin,actual_balance,email,phone,contact ORDER BY accno ASC;";
      $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
      $numrows = pg_num_rows($result);
      for($ri = 0; $ri < $numrows; $ri++) {
        echo "<tr>";
        $row = pg_fetch_assoc($result, $ri);
           echo "<td>",$row['accno'], "</td>
        <td>", $row['name'], "</td>
        <td>", $row['pin'], "</td>
        <td>", $row['actual_balance'], "</td>
        <td>", $row['email'], "</td>
        <td>", $row['phone'], "</td>
        <td>", $row['contact'], "</td>
      </tr>";
    }
    $query ="SELECT sum(actual_balance) as total FROM supplier";
    $result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
    $rows = pg_fetch_assoc($result);

  }

  pg_close($conn);
    ?>
    </tbody>
	</table>
</div>
<!-- /.card-body -->
<div class="card-footer">
 
  <div class="float-sm-right">
    <input type="text" name="rows" value="<?php $format = number_format($numrows);
                    echo($format); ?>"  style="width: 70px;text-align: right;">
    <input type="text" name="total" value="<?php $blc = number_format($rows['total'],3);
                    echo($blc); ?>"     style="background-color: lightgreen; text-align: right;">
  </div>
  </div>
  <!-- card.footer-->
</form>
 <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    /*$('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });*/
  });
</script>