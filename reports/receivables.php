<!DOCTYPE html>
<?php session_start();
require '../config/connection.php'; ?>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo isset($_SESSION['company']['name']) ? $_SESSION['company']['name'] : '' ?></title>

	<?php include '../header.php' ?>
	<style type="text/css">
		body{
			background-color: #f8f8ff;
		}
		input{
			padding: 1px;
			margin-left: 0px;
			border-style: double;
			border-color: inherit;
			border-width: 0.1rem;
			background-color: #f8f8ff;
			border-bottom-right-radius: 9px;
			border-bottom-left-radius: 9px;
			border-top-right-radius: 9px;
			border-top-left-radius: 9px;
		}
		@media (min-width: 480px){
			.card{
				margin-left: 6rem;
				margin-right: 6rem;
			}
			body{
				width: 100%;
			}
		}
	</style>
	<?php if (isset($_GET['id'])) {
		$id = $_GET['id'];

		$qry = pg_query($conn, "SELECT * FROM st_trans WHERE doc_no = '$id'") or die(pg_last_error($conn));
		$row = pg_fetch_assoc($qry);

		$dsql = pg_query($conn, "SELECT * FROM st_trans_details WHERE doc_no = '$id'") or die(pg_last_error($conn));

	} 
	?>
</head>
<body class="hold-transition text-sm">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Goods Received Note</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <div class="breadcrumb float-sm-right">
              <p class="breadcrumb-item"><b>Doc No.</b> <?php echo $id ?></p>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

	<section class="content">
		<div class="content-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="card shadow-lg">
						<div class="card-header">
							<div class="float-sm-left">
							Doc No. <input type="text" name="" id="input" value="<?php echo $id ?>" readonly>
							Date <input type="datetime" name="" id="input" value="<?php $date = date('d-m-Y', strtotime($row['date'])); echo $date ?>" readonly>
							</div>
							<div class="float-sm-right">
							A/C No. <input type="text" name="" id="input" style="width: 50px;" value="<?php echo $row['accno'] ?>" readonly> 
							 <input type="text" name="" value="<?php echo $row['supplier_name'] ?>" readonly>
							Posted At: <input type="datetime" name="" id="input" value="<?php echo $row['input_date'] ?>" readonly>
							</div>
							</div>
						<div class="card-body table-responsive p-0">
							<table class="table table-sm table-bordered table-striped table-hover text-nowrap">
								<thead>
									<tr>
										<td>#</td>
										<td>Code</td>
										<td>Description</td>
										<td>Qty</td>
										<td>Unit</td>
										<td>Vat</td>
										<td>Cost Price</td>
										<td>Goods Value</td>
										<td>VAT</td>
										<td>Amount</td>
									</tr>
								</thead>
								<tbody>
										<?php 
										$numrows = pg_num_rows($dsql);
										for($ri = 0; $ri < $numrows; $ri++) {
									 	echo "<tr>";
									 	$drow = pg_fetch_assoc($dsql);
      							echo "
										 <td>", $ri, "</td>
										 <td>", $drow['stockcode'], "</td>
										 <td>", $drow['description'], "</td>
										 <td>", $drow['qty'], "</td>
										 <td>", $drow['unit'], "</td>
										 <td>", $drow['vatcode'], "</td>
										 <td>", number_format($drow['vatunitpr'],2), "</td>
										 <td>", number_format($drow['lngoods'],2), "</td>
										 <td>", number_format($drow['lnvat'],2), "</td>
										 <td>", number_format($drow['lntotal'],2), "</td>
										</tr>";
									}
										?>
								</tbody>
							</table>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-sm-4">
							Totals:
							<?php echo $numrows ?>
						</div>
							<div class="col-sm-7">
								<div class=" float-sm-right">
								<input type="text" name="" id="input" value="<?php echo number_format($row['tlngoods'],2) ?>" readonly>
								<input type="text" name="" id="input" style="width: 60px;" value="<?php echo number_format($row['tlnvat'],2) ?>" readonly>
								<input type="text" name="" id="input" value="<?php echo number_format($row['tlntotal'],2) ?>" readonly>
							</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-4">
								<div class="float-sm-left">
								<button type="button" class="btn btn-sm btn-default"><i class="fa fa-print"></i> Print</button>
							</div>
							</div>
							<div class="col-sm-7">
								<div class="float-sm-right">
								<button type="button" id="myButton" class="btn btn-sm btn-info" style="background-color: grey;">Cancel</button>
							</div>
							</div>
						</div>
						</div>
					</div>
				</div>
				</div>
			</div>
	</section>

	<?php include '../scripts.php' ?>
	<script type="text/javascript">
		
	
		// Create a div element
const fakeEle = document.createElement('div');

// Hide it completely
fakeEle.style.position = 'absolute';
fakeEle.style.top = '0';
fakeEle.style.left = '1px';
fakeEle.style.overflow = 'hidden';
fakeEle.style.visibility = 'hidden';
fakeEle.style.whiteSpace = 'nowrap';
fakeEle.style.height = 'auto';
fakeEle.style.width = 'autoFill';

// We copy some styles from the textbox that effect the width
const textboxEle = document.getElementById('input');

// Get the styles
const styles = window.getComputedStyle(textboxEle);

// Copy font styles from the textbox
fakeEle.style.fontFamily = styles.fontFamily;
fakeEle.style.fontSize = styles.fontSize;
fakeEle.style.fontStyle = styles.fontStyle;
fakeEle.style.fontWeight = styles.fontWeight;
fakeEle.style.letterSpacing = styles.letterSpacing;
fakeEle.style.textTransform = styles.textTransform;

fakeEle.style.borderLeftWidth = styles.borderLeftWidth;
fakeEle.style.borderRightWidth = styles.borderRightWidth;
fakeEle.style.paddingLeft = styles.paddingLeft;
fakeEle.style.paddingRight = styles.paddingRight;

// Append the fake element to `body`
document.body.appendChild(fakeEle);
const setWidth = function () {
    const string = textboxEle.value || textboxEle.getAttribute('placeholder') || '';
    fakeEle.innerHTML = string.replace(/\s/g, '&' + 'nbsp;');

    const fakeEleStyles = window.getComputedStyle(fakeEle);
    textboxEle.style.width = fakeEleStyles.width;
};
setWidth();

textboxEle.addEventListener('input', function (e) {
    setWidth();
});
	</script>
</body>
</html>