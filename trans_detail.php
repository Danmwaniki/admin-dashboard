<?php include '../config/conn.php';
$type = $_SESSION['type'];

$dperiod = $_SESSION['select'];

$qry = "SELECT DISTINCT(st_trans_details.type),st_type.short_desc, COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount 
                  FROM st_trans_details JOIN st_type on st_type.type = st_trans_details.type WHERE st_type.group_desc='$type' AND st_trans_details.period = '$dperiod' group by st_trans_details.type,st_type.short_desc,st_type.sign";
                  $results = pg_query($conn, $qry) or die (pg_last_error($conn));
                 
                  $numrows = pg_num_rows($results)
?>


 <style type="text/css">
  input{
    background-color: lightgrey;
    text-align: right;
    border-width: 0.5px;
  }
</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Transaction Details</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index2.php">Home</a></li>
              <li class="breadcrumb-item"><a href="index2.php?page=trans_list" style="color: inherit;">trans_list</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <form method="post">
              <div class="card-header">
                Period:
                <input type="text" name="period" value="<?php echo $_SESSION['select']; ?>">
                
                  Transaction:
                  <select name="type">
                   <?php
                for($ri = 0; $ri < $numrows; $ri++) {
                   $d_row = pg_fetch_assoc($results);
                 echo"<option>",$d_row['short_desc'],"</option>";
                   }
                   ?>
                  </select>
                  <button class="btn btn-sm btn-success float-right" name="refresh">Refresh <i class="fa fa-sync-alt"></i></button>
              </div>
            </form>
              <div class="card-body">
                <table id="example2" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <td>Date:</td>
                      <td>Type:</td>
                      <td>Doc No:</td>
                      <td>Invoice No:</td>
                      <td>Name</td>
                      <td>lngoods:</td>
                      <td>lnVat:</td>
                      <td>lntotal:</td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(isset($_POST['refresh'])){
                      $period = $_POST['period'];
                      $type = $_POST['type'];

                 $qry = "SELECT st_trans_details.date, st_trans_details.type, st_trans_details.doc_no,st_trans_details.invno,st_trans_details.name,sum(st_trans_details.lngoods) as lngoods,sum(st_trans_details.lnvat)as lnvat,sum(st_trans_details.lntotal) as lntotal FROM st_trans_details JOIN st_type ON st_type.type = st_trans_details.type WHERE st_type.short_desc ='$type'and st_trans_details.period = '$period' GROUP BY st_trans_details.date, st_trans_details.type, st_trans_details.doc_no,st_trans_details.invno,st_trans_details.name";
                  $results = pg_query($conn, $qry) or die (pg_last_error($conn));
                   }
                   else{
                    return 0;
                   }
                ?>

                <?php $numrows = pg_num_rows($results);
                  for($ri = 0; $ri < $numrows; $ri++) {
                     
                    echo "<tr>";
                    $row = pg_fetch_assoc($results);
                    $doc_no = $row['doc_no'];
                    echo "
                    <td >", $row['date'], "</td>
                    <td>", $row['type'], "</td>
                    <td ><a href= 'index2.php?page=details' id=$doc_no>", $row['doc_no'], "</a></td>
                    <td>", $row['invno'], "</td>
                    <td >", $row['name'], "</td>
                    <td align=right>",number_format($row['lngoods'],2), "</td>
                    <td align=right>",number_format($row['lnvat'],2), "</td>
                    <td align=right>",number_format($row['lntotal'],2), "</td>
                    
                  </tr>";

                  }
                  ?>
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                <div class="float-sm-right">
                  <?php $qry = "SELECT sum(data.amount) as cost FROM (SELECT COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount 
                  FROM st_trans_details 
                  JOIN st_type on st_type.type = st_trans_details.type
                  where st_type.short_desc ='$type' and st_trans_details.period ='$period'
                  GROUP BY st_type.sign)as data";
                  $result = pg_query($conn, $qry) or die (pg_last_error($conn));
                  $row = pg_fetch_assoc($result);
                   ?>
                  Cost:
                  <input type="text" name="cost" value="<?php echo number_format($row['cost'],2); ?>">
                  <?php $qry = "SELECT sum(data.amount) as total FROM (SELECT COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lntotal)
                    ELSE
                      sum(st_trans_details.lntotal)*-1
                  END,0) as amount 
                  FROM st_trans_details 
                  JOIN st_type on st_type.type = st_trans_details.type
                  where st_type.short_desc ='$type' and st_trans_details.period ='$period'
                  GROUP BY st_type.sign)as data";
                  $result = pg_query($conn, $qry) or die (pg_last_error($conn));
                  $row = pg_fetch_assoc($result);
                   ?>
                  Total:
                  <input type="text" name="lntotal" value="<?php echo number_format($row['total'],2); ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>