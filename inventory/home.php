<?php require '../config/connection.php';?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Inventory</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index2.php?page=home">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-light elevation-1"><i class="far fa-calendar-check"></i></span>

              <div class="info-box-content">
                <?php
                    $period = $_SESSION['period'];
                 ?>
                <span class="info-box-text">Current Period</span>
                <span class="info-box-number">
                  <?php $newDate = date("d-m-Y", strtotime($period));
                   echo $newDate; ?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
           <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-share"></i></span>

              <div class="info-box-content">
                
                <?php 
                $query="SELECT COALESCE(sum(data.amount),0) as bal_bf from (SELECT a.sub_type,sum(a.amount ) as amount FROM(SELECT sub_type,
                      CASE 
                        WHEN st_type.sign = '+' THEN
                          sum(st_trans_details.lncost)
                        ELSE
                          sum(st_trans_details.lncost)*-1
                      END as amount
                       FROM st_type 
                       JOIN
                       st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period < '$period'
                        GROUP BY sub_type,st_type.sign ORDER BY sub_type) a GROUP BY sub_type)as data";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                    $row=pg_fetch_assoc($result);
                    $bal_bf = $row['bal_bf'];
                 ?>
                <span class="info-box-text">Balance BF</span>
                <span class="info-box-number">
                  <?php echo number_format($bal_bf,2) ?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-primary elevation-1"><i class="far fa-chart-bar"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Current Transactions</span>
                        <?php 
                $query="SELECT COALESCE(sum(data.amount),0) as cltotal from 
                        (SELECT a.sub_type,sum(a.amount ) as amount FROM(SELECT sub_type,
                      CASE 
                        WHEN st_type.sign = '+' THEN
                          sum(st_trans_details.lncost)
                        ELSE
                          sum(st_trans_details.lncost)*-1
                      END as amount
                       FROM st_type 
                       JOIN
                       st_trans_details ON st_trans_details.type = st_type.type WHERE st_trans_details.period = '$period'
                        GROUP BY sub_type,st_type.sign ORDER BY sub_type) a GROUP BY sub_type) as data
                        
                        ";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                    $row=pg_fetch_assoc($result);
                    $current = $row['cltotal'];
                 ?>
                <span class="info-box-number"><?php echo number_format($current,2); ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class=" fas fa-cart-arrow-down"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Current Sales</span>
                        <?php 
                        $sales = $bal_bf + $current;
                 ?>
                <span class="info-box-number"><?php echo number_format($sales,2); ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->


          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-chart-line"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Advanced Transactions</span>
                     <?php 
                     $query="SELECT COALESCE(sum(adv.advance),0) as sales FROM (SELECT a.sub_type,st_type.group_desc,sum(a.amount) as advance FROM (SELECT sub_type,st_type.sign,
                      CASE 
                      WHEN st_type.sign = '+' THEN
                         sum(st_trans_details.lncost)
                      ELSE
                        sum(st_trans_details.lncost)*-1
                      END as amount
                      FROM  st_type
                      JOIN st_trans_details ON st_type.type = st_trans_details.type 
                      WHERE st_trans_details.period > (select period FROM sys_trans_master WHERE sys_trans_master.type = 'ST') group by sub_type,st_type.sign order by sub_type) as a join st_type on a.sub_type=st_type.sub_type group by a.sub_type,st_type.group_desc)as adv";
                    $result = pg_query($conn, $query) or die (pg_last_error($conn));
                    $row=pg_fetch_assoc($result);
                    $advanced = $row['sales'];
                 ?>
                <span class="info-box-number"><?php echo number_format($advanced,2); ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-coins"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Actual Stock Value</span>
                <span class="info-box-number"><?php 
                      $actual = $sales + $advanced;
                      echo number_format($actual,2) ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <div class="col-md-6">
            <form method="post">
            <div class="card">
              <div class="card-header">
            <span class="info-box-text">Period:
              <?php $qry = "SELECT DISTINCT(period) FROM st_trans_details ORDER BY period DESC";
              $rlt = pg_query($conn, $qry) or die (pg_last_error($conn));
              $numrows = pg_num_rows($rlt);
               ?>
              <select style="border-width: 1px;border-color: dimgrey;padding: 2px;" name="period"><?php
              for($ri = 0; $ri < $numrows; $ri++) {
                $row=pg_fetch_assoc($rlt);
               echo"<option>",$row['period'],"</option>";
             }
             ?>
              </select>
              <button class="btn btn-info btn-sm float-right" name="refresh">Refresh <i class="fas fa-sync"></i></button>
            </span>
          </div>
          </div>
        </form>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-md-12">
            
                <div class="row">
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-header">
                    <p class="text-center">
                     Sales for Period: <?php if(isset($_POST['refresh'])){$newDate = date("d-m-Y", strtotime($_POST['period'])); 
                        echo $newDate;}else{
                          $newDate = date("d-m-Y", strtotime($period)); 
                        echo $newDate;
                        } ?>
                    </p>
                  </div>
                  <div class="card-body">
                 <?php 
                 if (isset($_POST['refresh'])) {

                   $selected_period= $_POST['period'];
                 

                   $query="SELECT a.sub_type,a.group_desc,COALESCE(sum(a.amount ),0) as current FROM(SELECT sub_type,group_desc,
                  COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount
                   FROM st_type 
                   JOIN
                   st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period = '$selected_period'
                    GROUP BY sub_type,group_desc,st_type.sign ORDER BY sub_type) a GROUP BY sub_type,group_desc ORDER BY current ASC";
                  $results = pg_query($conn, $query) or die (pg_last_error($conn));
                    $chart_data="";
                     while ($rows = pg_fetch_array($results)) { 
             
                        $type[]  = $rows['sub_type'];
                        $amount[] = $rows['current'];
                    }
                  }
                  else
                  {
                    $query="SELECT a.sub_type,a.group_desc,COALESCE(sum(a.amount ),0) as current FROM(SELECT sub_type,group_desc,
                  COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount
                   FROM st_type 
                   JOIN
                   st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period = '$period'
                    GROUP BY sub_type,group_desc,st_type.sign ORDER BY sub_type) a GROUP BY sub_type,group_desc ORDER BY current ASC";
                  $results = pg_query($conn, $query) or die (pg_last_error($conn));

                    $chart_data="";
                     while ($rows = pg_fetch_array($results)) { 
             
                        $type[]  = $rows['sub_type'];
                        $amount[] = $rows['current'];
                    }
                  }
                    ?>
                 
                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                        <canvas id="chartjs_bar" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        
                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                  </div>
                </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-header">
                   <div style="text-align: center;">
                     Transaction for Period: <?php if(isset($_POST['refresh'])){$newDate = date("d-m-Y", strtotime($_POST['period'])); 
                        echo $newDate;}else{
                          $newDate = date("d-m-Y", strtotime($period)); 
                        echo $newDate;
                        } ?>
                    </div>
                  </div>
                  <div class="card-body">
                    <from method="post">
                   <table class="table table-bordered table-striped table-hover">
                     <thead>
                       <tr>
                         <td>Type</td>
                         <td>Description</td>
                         <td>Amount</td>
                         <td>Action</td>
                       </tr>
                     </thead>
                     <tbody>
                       <?php 
                 if (isset($_POST['refresh'])) {

                  $gperiod = $_POST['period'];

                  $_SESSION['select'] = $gperiod;

                   $query="SELECT a.sub_type,a.group_desc,COALESCE(sum(a.amount ),0) as current FROM(SELECT sub_type,group_desc,
                  COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount
                   FROM st_type 
                   JOIN
                   st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period = '$gperiod'
                    GROUP BY sub_type,group_desc,st_type.sign ORDER BY sub_type) a GROUP BY sub_type,group_desc ORDER BY current ASC";
                  $results = pg_query($conn, $query) or die (pg_last_error($conn));
                     while($row= pg_fetch_array($results)){

                        ?>
                     <tr>
                    <td name='sub_type' ><?echo $row['sub_type'] ?></td>
                    <td><?php echo $row['group_desc'] ?></td>
                    <td align=right><?php  echo number_format($row['current'],2); ?></td>
                    <td><a href='index2.php?page=trans_list' class='btn btn-primary btn-sm' name='sub_type'>view</a></td>
                  </tr>
                  <?php
                  } 
                  }
                  else
                  {
                     $_SESSION['select'] = $period;

                    $query="SELECT a.sub_type,a.group_desc,COALESCE(sum(a.amount ),0) as current FROM(SELECT sub_type,group_desc,
                  COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount
                   FROM st_type 
                   JOIN
                   st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period = '$period'
                    GROUP BY sub_type,group_desc,st_type.sign ORDER BY sub_type) a GROUP BY sub_type,group_desc ORDER BY current ASC";
                  $results = pg_query($conn, $query) or die (pg_last_error($conn));
                  while($row= pg_fetch_array($results)){

                        ?>
                        <tr>

                    <td name='sub_type'><?php echo $row['sub_type'] ??= 'Trans Type' ?></td>
                    <td><?php echo $row['group_desc'] ??= 'Type Description' ?></td>
                    <td align=right><?php echo number_format($row['current'],2) ?></td>
                    <td><a href='index2.php?page=trans_list' class='btn btn-primary btn-sm' name='sub_type'>view</a></td>
                  </tr>
                  <?php
                  }

               }
                    ?>
                     </tbody>
                   </table>
                    </from>
                  </div>
                </div>
                  </div>
                  <!-- /.col -->
                </div>
                </div>
                <!-- /.row -->
              </div>
            </div>
          </section>
        </div>

  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>


<script type="text/javascript">
      var ctx = document.getElementById("chartjs_bar").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        showInLegend: 'true',
                        xLabel: 'Type',
                        labels:<?php echo json_encode($type); ?>,
                        datasets: [{
                            backgroundColor: "#2ec551",
                            borderColor: '#007bff',
                            pointBorderColor: '#007bff',
                            pointBackgroundColor: '#007bff',
                            fill: true,
                            data:<?php echo json_encode($amount);?>,
                        }]
                    },
                    options: {
                           legend: {
                        display: true,
                        position: 'bottom',
 
                        labels: {
                            fontColor: '#71748d',
                            fontFamily: 'Circular Std Book',
                            fontSize: 14,

                        }
                    },
 
 
                }
                });
  </script>
   