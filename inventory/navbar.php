
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index2.php?page=home" class="nav-link">Home</a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="account_settings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
          <i class="far fa-user mr-1"></i>
          <?php echo $_SESSION['login_name']; ?>
        </a>
        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="account_settings">
          <a href="javascript:void(0)" class="dropdown-item" id="manage_my_account">
            <!-- Message Start -->
            <i class="fa fa-cog mr-2"></i> 
                 Account Details
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
                 <i class="fas fa-building mr-2"></i>
                 Company Details
          </a>
          <div class="dropdown-divider"></div>
          <a onClick="window.open('../config/connTest.php')" class="dropdown-item">
            <!-- Message Start -->
                 <i class="fa fa-database mr-2"></i>
                 Database
          </a>
          <div class="dropdown-divider"></div>
          <a href="../ajax.php?action=logout" class="dropdown-item">
            <!-- Message Start -->
                <i class="fas fa-power-off mr-2"></i>
                Logout
          </a>
          
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
        <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
<script>
  $('#manage_my_account').click(function(){
    uni_modal("Manage Account","../manage_account.php?id=<?php echo $_SESSION['login_id'] ?>&mtype=own")
  })
</script>