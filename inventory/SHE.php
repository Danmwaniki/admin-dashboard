<?php 
include '../config/conn.php';

if(isset($_GET['page'])){

  $type = $_GET['page'];
$query = "SELECT stockcode,description,qty,unitcostv,qty*unitcostv as value FROM st_trans_details WHERE type='$type'";
$result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
$qry = "SELECT long_desc FROM st_type WHERE type='$type' ";
$desc = pg_exec($conn, $qry) or die('Error message: ' . pg_last_error());

}
?>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Stock Details</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index2.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
               
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>Stockcode</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Goods Cost</th>
                <th>Goods Value</th>
              </tr>
            </thead>
            <tbody><?php
               $numrows = pg_num_rows($result);
      for($ri = 0; $ri < $numrows; $ri++) {
        echo "<tr>";
        $row = pg_fetch_assoc($result);
        echo "
        <td>", $row['stockcode'], "</td>
        <td>", $row['description'], "</td>
        <td>",$row['qty'], "</td>
        <td>", $row['unitcostv'], "</td>
        <td>", $row['value'], "</td>
      </tr>";
    }
    $q_1 ="SELECT sum(unitcostv) as cost FROM st_trans_details WHERE type='$type'";
    $q1 = pg_exec($conn, $q_1) or die('Error message: ' . pg_last_error());
    $cost = pg_fetch_assoc($q1);

    $q_2 ="SELECT sum(qty) *sum(unitcostv) as value FROM st_trans_details WHERE type='$type'";
    $q2 = pg_exec($conn, $q_2) or die('Error message: ' . pg_last_error());
    $value = pg_fetch_assoc($q2);

    pg_close($conn);
    ?>
    </tbody>
  </table>
</div>
<!-- /.card-body -->
<!-- /.card-body -->
<div class="card-footer">
 
  <div class="float-sm-right">
    <input type="text" name="rows" value="<?php $format = number_format($numrows);
                    echo($format); ?>" readonly="" style="width: 70px;text-align: right;">
    <input type="text" name="cost" value="<?php $costv = number_format($cost['cost'],2);
                    echo($costv); ?>" readonly=""    style="background-color: lightgreen; text-align: right;">
    <input type="text" name="value" value="<?php $valu = number_format($value['value'],2);
                    echo($valu); ?>" readonly=""    style="background-color: lightgrey; text-align: right;">
    
 </div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    /*$('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });*/
  });
</script>