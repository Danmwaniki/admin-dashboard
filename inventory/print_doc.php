<?php 
session_start();

include '../config/connection.php';

if (isset($_GET['doc'])) {
  $doc_no = $_GET['doc'];
  $period = $_SESSION['select'];

  $qry = "SELECT company.name as company,branch.name as branch,address,phone,pin_no,vat_no FROM company left join branch on branch.code = company.branch";
  $result = pg_query($conn, $qry) or die (pg_last_error($conn));
  $row = pg_fetch_assoc($result);

  $query = "SELECT  supplier.address as address,branch.name as branch,st_trans_details.date,st_trans_details.accno,st_trans_details.name, st_trans_details.vat_rate,st_trans_details.vatcode,st_trans_details.invno,st_trans_details.ref,st_trans_details.stockcode, st_trans_details.description,st_trans_details.qty,st_trans_details.unitcost,st_trans_details.lntotal,st_trans_details.sprice1,st_trans_details.margin
from st_trans_details
LEFT JOIN branch ON st_trans_details.brcode = branch.code
LEFT JOIN company ON branch.code= company.branch
LEFT JOIN supplier ON st_trans_details.accno = supplier.code
WHERE st_trans_details.doc_no = '$doc_no'";

  $results = pg_query($conn, $query) or die (pg_last_error($conn));
  $row_2 = pg_fetch_assoc($results);
}

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Printing.....</title>
	 <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

   
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

<style type="text/css">
	.header{
		text-align: center;
	}
</style>
</head>
<body class="text-sm">
     <div class="content-fluid">
        <form class="center-form">
          <!-- Main content -->
        <div class="invoice p-3 mb-3">
        <div class="row">
          <div class="col-12">
          <div class="header">
            <h2><b><?php echo $row['company']; ?></b></h2>
              <h5><?php echo $row['address'] ?>, TEL: <?php echo $row['phone'] ?>, <b><?php echo $row['branch'] ?></b>,</h5>
              <h5><?php echo $_SESSION['doc_type']; ?></h5>
            </div>
          </div>
        </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-7 invoice-col" align="left">
          <address>
            PIN : <?php echo $row['pin_no']; ?><br><br>
            A/C NO :<?php echo $row_2['accno'] ?><br>
            Name : <?php echo $row_2['name'] ?><br>
            Address : <?php echo $row_2['address'] ?><br> 
          </address>
        </div>
        <!-- /.col -->
      <div class="col-sm-4 invoice-col" align="right">
        <address>
        Vat: <?php echo $row['vat_no'] ?><br><br>
        <b>Doc No:  <?php echo $doc_no ?></b><br>
        Date: <?php echo $row_2['date']; ?><br> 
        Del No: <?php  ?><br>
        Invoice No:   <?php echo $row_2['invno'] ?><br>
        Ref No: <?php echo $row_2['ref'] ?><br>
        </address>
      </div>
      <!-- /.col -->
    </div>
    <!-- row -->
    <br><br>
    <!-- Table row -->
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <td>Ln.</td>
              <td>Code</td>
              <td>Description</td>
              <td>Qty/Pcs</td>
              <td>C/Price</td>
              <td>S/Price</td>
              <td>Amount</td>
              <td>Vat</td>
              <td>Mrg%</td>
            </tr>
          </thead>
          <tbody>
           <?php
           $i = 1;
            while($d_row= pg_fetch_array($results)){
                        ?>
            <tr>

              <td><?php echo $i++ ?></td>
              <td><?php echo $d_row['stockcode'] ?></td>
              <td><?php echo $d_row['description'] ?></td>
              <td><?php echo $d_row['qty'] ?></td>
              <td><?php echo $d_row['unitcost'] ?></td>
              <td><?php echo $d_row['sprice1'] ?></td>
              <td><?php echo $d_row['lntotal'] ?></td>
              <td><?php echo $d_row['vatcode'] ?></td>
              <td><?php echo $d_row['margin'] ?></td>
            </tr>
            <?php 
          } ?>
          </tbody>
        </table>
      </div>
      <!-- col -->
    </div>
    <!-- row-->
    <hr>
    <div class="row">
      <!-- VAT ANALYSIS -->
      <div class="col-6">
        <p>VAT ANALYSIS</p>
        <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <td>VAT</td>
              <td>Goods Value</td>
              <td>Vat</td>
              <td>Total</td>
            </tr>
          </thead>
          <tbody>
            <?php
            $qrry = "SELECT DISTINCT(vatcode), sum(lngoods) as goods, sum(lnvat) as vat, sum(lntotal) as total FROM st_trans_details where doc_no = '$doc_no' GROUP BY vatcode;";
              $reslt = pg_query($conn, $qrry) or die (pg_last_error($conn));
            while($vat_row = pg_fetch_array($reslt)){

              ?>
            <tr>
              <td><?php echo $vat_row['vatcode'] ?></td>
              <td><?php echo number_format($vat_row['goods'],2) ?></td>
              <td><?php echo number_format($vat_row['vat'],2) ?></td>
              <td><?php echo number_format($vat_row['total'],2) ?></td>
            </tr>
          <?php }?>
          </tbody>
        </table>
      </div>
      <!-- div table -->
      </div>
      <!-- col -->
      <div class="col-6">
        <div class="table-responsive">
          <table class="table">
            <?php $qery = "SELECT sum(lngoods) as goods, sum(lnvat) as vat, sum(lntotal) as total FROM st_trans_details where doc_no = '$doc_no'";
            $rslt = pg_query($conn, $qery) or die (pg_last_error($conn));
           $sum_row = pg_fetch_assoc($rslt);

             ?>
            <tr>
              <th style=" width: 50%;">
                <b>Goods Value:</b>
              </th>
              <td align="right"><b><?php echo number_format($sum_row['goods'],2); ?></b></td>
            </tr>
            <tr>
              <th>Vat Value:</th>
              <td align="right"><b><?php echo number_format($sum_row['vat'],2); ?></b></td>
            </tr>
            <th>Total Value:</th>
            <td align="right"><b><?php echo number_format($sum_row['total'],2); ?></b></td>
          </table>
        </div>
        <!-- div table -->
      </div>
      <!-- col -->
    </div>
    <!-- row -->
    <br>
    <div class="row">
      <div class="col-6">
      Input By: <?php ?>
    </div>
    <div class="col-6">
      Sign: ..............................................

      Checked by: ........................................
    </div>
    </div>
    <!-- /.row -->
<br>
<hr>
  
  </div>
  <!-- invoice -->
      </form>
      <!-- /.form -->
      </div>
      <!-- /.content -->
<!-- Page specific script -->
<script>
  window.addEventListener("load", window.print());
</script>
</body>
 <!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->

</html>