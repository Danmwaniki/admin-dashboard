<?php include '../config/connection.php';
if (!isset($_SESSION['period'])) {
  echo "<script language='javascript'>location.href'index2.php'</script>";
}
 ?>

<style type="text/css">
  input{
    background-color: #DCDCDC;
    border-color: lightgrey;
    text-align: right;
    border-width: 0.5px;
  }
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Transaction Listing</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index2.php">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <form method="post">
              <div class="card-header">
                Period:
                <input type="text" name="details-period" value="<?php echo $_SESSION['select'] ? $_SESSION['select'] :'';?>" readonly="" style="width: 85px;">
                <?php $s_period = $_SESSION['select'];  ?>
                From:
                <input type="text" name="from" value="<?php echo date("Y-m-01" ,strtotime($s_period)); ?>" readonly="" style="width: 95px;">
                To:
                <input type="text" name="to" value="<?php echo date("Y-m-t" ,strtotime($s_period)); ?>" readonly="" style="width: 95px;">
                Transaction Type:
                <?php

        $pselect = $_SESSION['select'];

              $qery="SELECT a.sub_type,a.group_desc,COALESCE(sum(a.amount ),0) as current FROM(SELECT sub_type,group_desc,
                  COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount
                   FROM st_type 
                   JOIN
                   st_trans_details ON st_trans_details.type = st_type.type where st_trans_details.period = '$pselect'
                    GROUP BY sub_type,group_desc,st_type.sign ORDER BY sub_type) a GROUP BY sub_type,group_desc";
              $result = pg_query($conn, $qery) or die (pg_last_error($conn));
              $numrows = pg_num_rows($result);
            ?>
              <select name="type"><?php
              for($ri = 0; $ri < $numrows; $ri++) {
                $row=pg_fetch_assoc($result);
               echo"<option>",$row['group_desc'],"</option>";
             }
             ?>
                </select>
                 <button class="btn btn-sm btn-success float-right" name="view_trans" type="submit">View <i class="fas fa-sync"></i></button>
              </div>
            </form>
              <div class="card-body">
                <table id="example2" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <td>Trans_type</td>
                      <td>Description</td>
                      <td>Amount</td>
                      <td>Action</td>
                    </tr>
                    <tbody>
                       <?php  
                if(isset($_POST['view_trans'])){

                  $dperiod = $_POST['details-period'];
                  $fromdate = $_POST['from'];
                  $todate = $_POST['to'];
                  $type = $_POST['type'];
                  $_SESSION['type'] = $type;

                 $qry = "SELECT DISTINCT(st_trans_details.type),st_type.short_desc, COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount 
                  FROM st_trans_details JOIN st_type on st_type.type = st_trans_details.type WHERE st_type.group_desc='$type' AND st_trans_details.period = '$dperiod' AND st_trans_details.date BETWEEN '$fromdate' AND '$todate' group by st_trans_details.type,st_type.short_desc,st_type.sign";
                  $results = pg_query($conn, $qry) or die (pg_last_error($conn));

                  while($trans_row= pg_fetch_array($results)){

                        ?>
                        <tr>
                    <td ><?php echo $trans_row['type']; ?></td>
                    <td><?php echo $trans_row['short_desc']; ?></td>
                    <td align=right><?php echo number_format($trans_row['amount'],2); ?></td>
                    <td><a href='index2.php?page=trans_detail' class='btn btn-sm btn-warning' name='sub_type'>Details</a></td>
                  </tr>
                  <?php
                  } 
                   }
                   else{
                    return 0;
                   }
                ?>
                    </tbody>
                  </thead>
                </table> 
              </div>
              <div class="card-footer">
                <div class="float-sm-right">
                  <?php 
                  $query="SELECT sum(data.amount) as tlamount FROM (SELECT DISTINCT(st_trans_details.type),st_type.short_desc, COALESCE(CASE 
                    WHEN st_type.sign = '+' THEN
                      sum(st_trans_details.lncost)
                    ELSE
                      sum(st_trans_details.lncost)*-1
                  END,0) as amount 
                  FROM st_trans_details JOIN st_type on st_type.type = st_trans_details.type WHERE st_type.group_desc='$type' AND st_trans_details.period = '$dperiod' AND st_trans_details.date BETWEEN '$fromdate' AND '$todate' group by st_trans_details.type,st_type.short_desc,st_type.sign)as data";
                  $results = pg_query($conn, $query) or die (pg_last_error($conn));
                  $tlrow = pg_fetch_assoc($results);
                  ?>
                Total Amount
                <input type="text" name="tlamount" value="<?php echo number_format($tlrow['tlamount'],2); ?>" readonly>
                </div>
              </div>
            </div>
          </div>
        </div>
    
      </div>
    </section>
  </div>



