<?php 
include 'config/conn.php'; 
if(isset($_GET['page'])){
  $type = $_GET['page'];
$query = "SELECT stockcode,description,qty,unitcostv,qty*unitcostv as value FROM st_trans_details WHERE type='$type'";
$result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
$qry = "SELECT long_desc FROM st_type WHERE type='$type' ";
$desc = pg_exec($conn, $qry) or die('Error message: ' . pg_last_error());

}
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List of <?php
            $row = pg_fetch_assoc($desc);
             echo $row['long_desc'] ?>'s</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item"><a href="index.php?page=stock_mov">Stock Mov</a></li>
               <li class="breadcrumb-item active"><?php echo $type ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
               
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th>Stockcode</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Goods Cost</th>
                <th>Goods Value</th>
              </tr>
            </thead>
            <tbody><?php
               $numrows = pg_num_rows($result);
      for($ri = 0; $ri < $numrows; $ri++) {
        echo "<tr>";
        $row = pg_fetch_assoc($result);
        echo "
        <td>", $row['stockcode'], "</td>
        <td>", $row['description'], "</td>
        <td>",$row['qty'], "</td>
        <td>", $row['unitcostv'], "</td>
        <td>", $row['value'], "</td>
      </tr>";
    }
    $q_1 ="SELECT sum(unitcostv) as cost FROM st_trans_details WHERE type='$type'";
    $q1 = pg_exec($conn, $q_1) or die('Error message: ' . pg_last_error());
    $cost = pg_fetch_assoc($q1);

    $q_2 ="SELECT sum(qty) *sum(unitcostv) as value FROM st_trans_details WHERE type='$type'";
    $q2 = pg_exec($conn, $q_2) or die('Error message: ' . pg_last_error());
    $value = pg_fetch_assoc($q2);

    pg_close($conn);
    ?>
    </tbody>
  </table>
</div>
<!-- /.card-body -->
<!-- /.card-body -->
<div class="card-footer">
 
  <div class="float-sm-right">
    <input type="text" name="rows" value="<?php $format = number_format($numrows);
                    echo($format); ?>" readonly="" style="width: 70px;text-align: right;">
    <input type="text" name="cost" value="<?php $costv = number_format($cost['cost'],2);
                    echo($costv); ?>" readonly=""    style="background-color: lightgreen; text-align: right;">
    <input type="text" name="value" value="<?php $valu = number_format($value['value'],2);
                    echo($valu); ?>" readonly=""    style="background-color: lightgrey; text-align: right;">
    
  </div>
  </div>
  <!-- card.footer-->

</div>
 <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    /*$('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });*/
  });
</script>