<footer class="main-footer footer-fixed">
    <strong>Copyright &copy; 2021 Smartpos.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.101
    </div>
  </footer>