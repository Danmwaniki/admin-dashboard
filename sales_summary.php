<?php include 'config/connection.php'; ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Sales Summary</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <?php 
                  $query = pg_query($conn, "SELECT distinct(branch.name) FROM company LEFT JOIN branch ON company.branch = branch.code ;") or die(pg_last_error($conn));
                  $branch = pg_fetch_array($query);
                  foreach ($branch as $key => $value) {
                    $_SESSION['branch_'.$key] = $value;
                  }
                 ?>
                 <h3 class="card-title">Branch: <?php echo $_SESSION['branch_name']; ?>
              </h3>
                 <?php
                    $f_qry = pg_query($conn, "SELECT max(date) as c_date, date_trunc('month',max(date)) as f_date FROM eod;") or die(pg_last_error($conn));
                    $f_row = pg_fetch_assoc($f_qry);
                    $f_day = $f_row['f_date'];
                    $c_day = $f_row['c_date'];
                    ?>
              <div class="card-tools">
                  From: 
                  <input type="date" name="from" value="<?php $fromDate = date('d/m/Y', strtotime($f_day ??= 'default value')); 
                   echo $fromDate; ?>" autofocus>
                To: 
                <input type="date" name="to" value="<?php echo $cur_date_time = date('dd/mm/yyyy'); ?>" autofocus>
              </div>
              </div>
              <div class="card-body table-responsive p-0">
                <table class="table table-sm table-bordered table-striped table-hover text-nowrap">
                  <thead>
                    <tr>
                      <td>Date:</td>
                      <td>Custs:</td>
                      <td>Sales:</td>
                      <td>Collections:</td>
                      <td>Total Sales <br>+Collections</td>
                      <td>Total Cash <br>Picked</td>
                      <td>Cheques <br>Picked</td>
                      <td>Credit Cards <br> Picked</td>
                      <td>M/Money <br>Picked</td>
                      <td>Total <br> Banking:</td>
                      <td>Refunds <br> & Expenses:</td>
                      <td>Non Cash:</td>
                      <td>Variance:</td>
                    </tr>
                  </thead>
                  <tbody>
                 <?php

                    $query = "SELECT date,customers,sales,expenses,((COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) + sales ) as net_sales,(COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) as deposits,(pickups+COALESCE(cash_balance,0)) as pickups,credit_cards,m_pesa,banking, (banking+COALESCE(non_cash,0) - (COALESCE(net_sales,0) + COALESCE(deposits,0) + COALESCE(on_acc,0) + COALESCE(caps,0))) as variance,COALESCE(non_cash,0)  as cash_balance,cheque FROM eod  WHERE date::date BETWEEN '$f_day' AND '$c_day'  order by date ASC";
                  $results = pg_query($conn, $query) or die (pg_last_error($conn));
                  while($row= pg_fetch_array($results)){

                  ?>
                    <tr>
                      <td><?php echo date("d-m-Y" ,strtotime($row['date'])); ?></td>
                      <td><?php echo $row['customers']; ?></td>
                      <td><?php echo number_format($row['sales'],2); ?></td>
                      <td><?php echo number_format($row['deposits'],2); ?></td>
                      <td><?php echo number_format($row['net_sales'],2); ?></td>
                      <td><?php echo number_format($row['pickups'],2); ?></td>
                      <td><?php echo number_format($row['cheque'],2); ?></td>
                      <td><?php echo number_format($row['credit_cards'],2); ?></td>
                      <td><?php echo number_format($row['m_pesa'],2); ?></td>
                      <td><?php echo number_format($row['banking'],2); ?></td>
                      <td><?php echo number_format($row['expenses'],2); ?></td>
                      <td><?php echo number_format($row['cash_balance'],2); ?></td>
                      <td><?php echo number_format($row['variance'],2); ?></td>
                    </tr>
                    <?php
                  }
                    ?>
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                <a href="sales_print.php" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i>Print</a>
                <div class="float-sm-right">
                  <button class="btn btn-sm btn-success"><i class="fas fa-file-export"></i> Export</button>
                  <button class="btn btn-sm btn-danger"><i class="fa fa-file-pdf" aria-hidden="true"></i>  Pdf</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>



