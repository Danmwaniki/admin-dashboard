<!DOCTYPE html>
<?php session_start(); ?>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body class="hold-transition">
	<div class="content-fluid">
		<div class="invoice">
			<div class="row">
				<div class="col-12">
					<div class="header">
						<h2><b><?php echo $_SESSION['company']['name'] ?></b></h2>
						<h5><?php echo $_SESSION['company'][''] ?></h5>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Page specific script -->
<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>