<!DOCTYPE html>
<?php 
  session_start();
?>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Configuration || page</title>

   <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="../plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- BS Stepper -->
  <link rel="stylesheet" href="../plugins/bs-stepper/css/bs-stepper.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
    <a href="#"><b>Smart</b>POS</a>
  </div>
  <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Please Provide configuration details</p>
        <form method="post">
          <div class="form-group">
             <select class="form-control select2" name="user" style="width: 100%;">
                <option selected="selected" value="admin">Admin</option>
                <option value="support">Support</option>
                <option value="postgres">Postgres</option>
              </select>
            </div>
            <div class="form-group">
              <div class="input-group mb-3">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
             <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          </div>
        </div>
        <div class="float-sm-right">
        <button type="submit" class="btn btn-sm btn-info" name="save">Save</button>
        <button type="reset" class="btn btn-sm btn-default" onclick="window.close()">Cancel</button>
      </div>
        </form>
      </div>
    </div>
  </div>

  <!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- BS-Stepper -->
<script src="../plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()


  })
  
</script>
</body>
</html>

<?php 
  if (isset($_POST['save'])) {
    extract($_POST);

      if (!file_exists("C:\Users\Danson\AppData\Roaming\smartpos\pos_backoff")) {
        mkdir("C:\Users\Danson\AppData\Roaming\smartpos\pos_backoff", 0777, true);
      }
    //add contents
    $myFile ='C:\Users\Danson\AppData\Roaming\smartpos\pos_backoff\config.bat';
    //first string
    $string = ($user."".$password);

     //--- ENCRYPTION ---
    
    $plaintext = base64_encode($user)."\n".base64_encode($password);

    $fd = file_put_contents($myFile,$plaintext);


    if (!$fd) {
      echo "<script languange='text/javascript'>alert('Error Inserting Details')</script>";
    }else{
     header('location:../login.php');
    }
  }
?>