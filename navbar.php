
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php?page=home" class="nav-link">Home</a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="account_settings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
          <i class="far fa-user mr-1"></i>
          <?php echo $_SESSION['login_name']; ?>
        </a>
        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="account_settings">
          <button type="button" class=" btn btn-default dropdown-item" data-toggle="modal" data-target="#modal-default">
            <!-- dropdown start -->
            <i class="fa fa-cog mr-2"></i> 
                 Account Details
          </button>
          <div class="dropdown-divider"></div>
          <button type="button" class="btn btn-default dropdown-item" data-toggle="modal" data-target="#modal-lg">
            <!-- Message Start -->
                 <i class="fas fa-building mr-2"></i>
                 Company Details
          </button>
          <div class="dropdown-divider"></div>
          <button type="button" class="btn btn-default dropdown-item" data-toggle="modal" data-target="#modal-default2">
            <!-- Message Start -->
                 <i class="fa fa-database mr-2"></i>
                 Database
          </button>
          <div class="dropdown-divider"></div>
          <a href="ajax.php?action=logout" class="dropdown-item">
            <!-- Message Start -->
                <i class="fas fa-power-off mr-2"></i>
                Logout
          </a>
          <div class="dropdown-divider"></div>
          <a href="lockscreen.php" class="dropdown-item">
            <!-- Message Start -->
                <i class="fas fa-lock mr-2"></i>
                Lock
          </a>
          
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

