<?php include 'config/connection.php'; ?>

   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">X Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index2.php?page=home">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <form method="post">
                <b>Run Date</b>
                <input type="date" name="rundate" placeholder="rundate">
                
                <div class="float-sm-right">
                  <button class="btn btn-sm btn-info" type="submit" name="refresh">Refresh</button>
                </div>
              </form>
              </div>
              <div class="card-body">
                <table id="example2" class="table table-sm table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <td>Receipt No:</td>
                      <td>Run Date:</td>
                      <td>Total:</td>
                      <td>CAS:</td>
                      <td>GIFT:</td>
                      <td>CAPS:</td>
                      <td>CRN:</td>
                      <td>ON/ACC:</td>
                      <td>DEP:</td>
                      <td>LOY:</td>
                      <td>CHQ:</td>
                      <td>VISA:</td>
                      <td>MPS:</td>
                      <td>Action:</td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if (isset($_POST['refresh'])) {
                      $run_date = date("Y-m-d" ,strtotime($_POST['rundate']));
                      $_SESSION['rec_date'] = $run_date;
                     
                    $results =pg_query($conn, "SELECT receipt_no,input_date,sum(amount) as total,
                        CASE 
                          WHEN type='CAS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CAS,CASE 
                          WHEN type='GIFT' THEN
                            sum(amount)
                          ELSE
                            0
                        END as GIFT,CASE 
                          WHEN type='CAPS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CAPS
                        ,CASE 
                          WHEN type='CRN' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CRN,CASE 
                          WHEN type='ACC' THEN
                            sum(amount)
                          ELSE
                            0
                        END as ON_ACC,CASE 
                          WHEN type='DEP' THEN
                            sum(amount)
                          ELSE
                            0
                        END as DEP,CASE 
                          WHEN type='LOY' THEN
                            sum(amount)
                          ELSE
                            0
                        END as LOY,CASE 
                          WHEN type='CHQ' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CHQ,CASE 
                          WHEN type='VIS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as VIS,CASE 
                          WHEN type='MPS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as MPS
                         FROM pos_payment_details
                         WHERE run_date='$run_date' 
                         GROUP BY receipt_no,input_date,type 
                         UNION
                         SELECT receipt_no,input_date,sum(amount) as total,
                        CASE 
                          WHEN type='CAS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CAS,CASE 
                          WHEN type='GIFT' THEN
                            sum(amount)
                          ELSE
                            0
                        END as GIFT,CASE 
                          WHEN type='CAPS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CAPS
                        ,CASE 
                          WHEN type='CRN' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CRN,CASE 
                          WHEN type='ACC' THEN
                            sum(amount)
                          ELSE
                            0
                        END as ON_ACC,CASE 
                          WHEN type='DEP' THEN
                            sum(amount)
                          ELSE
                            0
                        END as DEP,CASE 
                          WHEN type='LOY' THEN
                            sum(amount)
                          ELSE
                            0
                        END as LOY,CASE 
                          WHEN type='CHQ' THEN
                            sum(amount)
                          ELSE
                            0
                        END as CHQ,CASE 
                          WHEN type='VIS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as VIS,CASE 
                          WHEN type='MPS' THEN
                            sum(amount)
                          ELSE
                            0
                        END as MPS
                         FROM till_payment_details WHERE run_date='$run_date' GROUP BY receipt_no,input_date,type ORDER BY input_date") or die(pg_last_error($conn));


                         while($row = pg_fetch_array($results)){

                          ?>

                        <tr>
                       <td><a href="receipt_details.php?id=<?php echo $row['receipt_no']; ?>" style="color: black;"><?php echo $row['receipt_no']; ?></a></td>
                       <td><?php echo $row['input_date']; ?></td>
                       <td><?php echo number_format($row['total'],2); ?></td>
                       <td><?php echo number_format($row['cas'],2); ?></td>
                       <td><?php echo number_format($row['gift'],2); ?></td>
                       <td><?php echo number_format($row['caps'],2); ?></td>
                       <td><?php echo number_format($row['crn'],2); ?></td>
                       <td><?php echo number_format($row['on_acc'],2); ?></td>
                       <td><?php echo number_format($row['dep'],2); ?></td>
                       <td><?php echo number_format($row['loy'],2); ?></td>
                       <td><?php echo number_format($row['chq'],2); ?></td>
                       <td><?php echo number_format($row['vis'],2); ?></td>
                       <td><?php echo number_format($row['mps'],2); ?></td>
                       <td></td>
                     </tr>
                     <?php
                       }
                     }  
                     ?>
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
