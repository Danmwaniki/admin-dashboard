<?php include 'config/conn.php'; ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Stocks</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=home">Home</a></li>
              <li class="breadcrumb-item active">Stocks</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Stock Valuation</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
              	<th class="text-center">#</th>
                <th>Stockcode</th>
                <th>Description</th>
                <th>Bk/Stock</th>
                <th>Total Incl</th>
                <th>Pos Val Excl</th>
                <th>Pos Val Incl</th>
                <th>Margin %</th>
                <th>Margin Value</th>
              </tr>
            </thead>
						<tbody>
              

			<?php 
			$i = 1;
			$query = "select stockcode,description,abalstock,
ROUND((abalstock*unitcostv)::NUMERIC,2) as total_incl,
ROUND((abalstock*vsprice1)::NUMERIC,2) as pos_value_excl,
ROUND((abalstock*sprice1)::NUMERIC,2) as pos_value_incl,margin,
ROUND((abalstock*vsprice1)::NUMERIC,2) - ROUND((abalstock*unitcost)::NUMERIC,2) as margin_value
from stock limit 50";
			$result = pg_exec($conn, $query) or die('Error message: ' . pg_last_error());
			$numrows = pg_num_rows($result);
			
			?>
			<?php
			for($ri = 0; $ri < $numrows; $ri++) {
				echo "<tr>";
				$row = pg_fetch_assoc($result, $ri);
				echo "<td>", $i++, "</td>
				<td>", $row['stockcode'], "</td>
				<td>",$row['description'], "</td>
				<td>", $row['abalstock'], "</td>
				<td>", $row['total_incl'], "</td>
				<td>", $row['pos_value_excl'], "</td>
				<td>", $row['pos_value_incl'], "</td>
				<td>", $row['margin'], "</td>
				<td>", $row['margin_value'], "</td>
			</tr>";
		}
		pg_close($conn);
		?>
		</tbody>
	</table>
</div>
<!-- /.card-body -->
</div>
 <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": true,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    /*$('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });*/
  });
</script>