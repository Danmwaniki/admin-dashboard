<?php include 'config/connection.php'; ?>

<style type="text/css">
  input{
    background-color: #dcdcdc;
    border-width: 0.1px;
  }
</style>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           <h1 class="m-0">Day Sales Summary</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index2.php?page=home">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $page ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <form method="post">
              <div class="card-header">
                <div class="float-sm-left">
                  <?php
                  $query = "SELECT branch.name as branch FROM company LEFT JOIN branch ON company.branch = branch.code";
                  $results = pg_query($conn, $query) or die(pg_last_error($conn));
                  $row = pg_fetch_assoc($results);
                   ?>
                  Branch: 
                  <input type="text" name="branch" value="<?php echo $row['branch'] ?>" style="text-align: right;" readonly>
                  Date: 
                  <input type="date" name="date" class="text-sm" required>
                </div>
                <div class="float-sm-right">
                  <button type="submit" name="refresh" class="btn btn-sm btn-primary">Refresh</button>
                </div>
              </div>
            </form>
              <div class="card-body">
               
                <table class="table table-sm table-bordered table-striped table-hover text-sm" style="overflow: true; overflow-x: true; overflow-y: true;">
                  <thead>
                    <tr>
                      <td>Till:</td>
                      <td>Op:</td>
                      <td>Total<br>Sales:</td>
                      <td>Cash back:</td>
                      <td>Net<br> Sales:</td>
                      <td>Non-Cash:</td>
                      <td>Acc<br>Payment:</td>
                      <td>Deposits:</td>
                      <td>Caps<br>Paid:</td>
                      <td>P/Cash:</td>
                      <td>Pickups:</td>
                      <td>M/Money:</td>
                      <td>Visa<br> Card:</td>
                      <td>Banking:</td>
                      <td>Variance:</td>
                    </tr>
                  </thead>
                  <tbody>
                     <?php 
                if (isset($_POST['refresh'])) {
                  $date = $_POST['date'];

                  $query =pg_query($conn, "SELECT DISTINCT(a.till_no),a.op_code,(select coalesce(sum(amount),0) as amount from till_payment_details where run_date = a.run_date and till_no = a.till_no and amount > 0) as total,COALESCE(sum(a.crn),0) as cash_back,(select sum(amount) as amount from till_payment_details where run_date = a.run_date and till_no = a.till_no) as net,COALESCE(sum(a.non_cash),0) as non_cash, COALESCE(sum(a.acc),0) as acc,(select COALESCE(sum(amount),0) from deposits where till_no = a.till_no and date = a.run_date) as dep,COALESCE(sum(a.caps),0) as caps, COALESCE(sum(a.p_cash),0) as p_cash,(select COALESCE(sum(amount),0) as amount from pickup_trans where till = a.till_no and date = a.run_date) as pickups,COALESCE(sum(a.mps),0) as mps, COALESCE(sum(a.vis),0) as vis,COALESCE(sum(a.mps) + sum(a.vis) + (select COALESCE(sum(amount),0) as amount from pickup_trans where till = a.till_no and date = a.run_date),0 ) as banking ,COALESCE(sum(a.non_cash) + COALESCE(sum(a.mps) + sum(a.vis) + (select COALESCE(sum(amount),0) from deposits where  till_no = a.till_no and date = a.run_date) + (select COALESCE(sum(amount),0) as amount from pickup_trans where till = a.till_no and date = a.run_date),0),0) - (select sum(amount) as amount from till_payment_details where run_date = a.run_date and till_no = a.till_no) as variance FROM ( select DISTINCT(till_no),run_date,op_code,sum(amount) as total,
CASE 
  WHEN till_payment_details.type = 'ACC' THEN
    sum(amount)
  ELSE
    0
END as ACC,
CASE 
  WHEN till_payment_details.type = 'CAPS' THEN
    sum(amount)
  ELSE
    0
END as CAPS,
CASE 
  WHEN pay_type.type = 'CRN' AND till_payment_details.till_no = '99'
  THEN
    sum(amount)
  ELSE
    0
END as CRN,
CASE 
  WHEN till_payment_details.type = 'MPS' THEN
    sum(amount)
  ELSE
    0
END as MPS,
CASE 
  WHEN till_payment_details.type = 'VIS' THEN
    sum(amount)
  ELSE
    0
END as VIS,
CASE 
  WHEN pay_type.type='PCS' THEN
    sum(amount)
  ELSE
    0
END as p_cash,
CASE 
  WHEN pay_type.cash = 'f' THEN
    sum(amount)
  ELSE
    0
END as non_cash


from till_payment_details
LEFT JOIN pay_type ON till_payment_details.type = pay_type.type 
where till_payment_details.run_date ='$date' GROUP BY run_date,till_no,till_payment_details.type,pay_type.type,cash,till_payment_details.op_code,amount) as a
GROUP BY a.till_no,a.op_code,a.run_date ORDER BY till_no ASC;
") or die(pg_last_error($conn));
                  $rows = pg_num_rows($query);
                  if($rows > 1){
                    while($row = pg_fetch_assoc($query)){

                 ?>
                    <tr>
                      <td><?php echo $row['till_no'] ?></td>
                      <td><?php echo $row['op_code'] ?></td>
                      <td><?php echo number_format($row['total'],2) ?></td>
                      <td><?php echo number_format($row['cash_back'],2) ?></td>
                      <td><?php echo number_format($row['net'],2) ?></td>
                      <td><?php echo number_format($row['non_cash'],2) ?></td>
                      <td><?php echo number_format($row['acc'],2) ?></td>
                      <td><?php echo number_format($row['dep'],2) ?></td>
                      <td><?php echo number_format($row['caps'],2) ?></td>
                      <td><?php echo number_format($row['p_cash'],2) ?></td>
                      <td><?php echo number_format($row['pickups'],2) ?></td>
                      <td><?php echo number_format($row['mps'],2) ?></td>
                      <td><?php echo number_format($row['vis'],2) ?></td>
                      <td style="background-color: lightgreen;"><?php echo number_format($row['banking'],2) ?></td>
                      <td style="background-color: #ff6b6b;"><?php echo number_format($row['variance'],2) ?></td>
                    </tr>
                    <?php

                  }
                }
                  else{
                    $qury =pg_query($conn, "SELECT DISTINCT(a.till_no),a.op_code,(select coalesce(sum(amount),0) as amount from pos_payment_details where run_date = a.run_date and till_no = a.till_no and amount > 0) as total,COALESCE(sum(a.crn),0) as cash_back,(select sum(amount) as amount from pos_payment_details where run_date = a.run_date and till_no = a.till_no) as net,COALESCE(sum(a.non_cash),0) as non_cash, COALESCE(sum(a.acc),0) as acc,(select COALESCE(sum(amount),0) from deposits where till_no = a.till_no and date = a.run_date) as dep,COALESCE(sum(a.caps),0) as caps, COALESCE(sum(a.p_cash),0) as p_cash,(select COALESCE(sum(amount),0) as amount from pos_pickup_trans where till = a.till_no and date = a.run_date) as pickups,COALESCE(sum(a.mps),0) as mps, COALESCE(sum(a.vis),0) as vis,COALESCE(sum(a.mps) + sum(a.vis) + (select COALESCE(sum(amount),0) as amount from pos_pickup_trans where till = a.till_no and date = a.run_date),0 ) as banking ,COALESCE(sum(a.non_cash) + COALESCE(sum(a.mps) + sum(a.vis) + (select COALESCE(sum(amount),0) from deposits where  till_no = a.till_no and date = a.run_date) + (select COALESCE(sum(amount),0) as amount from pos_pickup_trans where till = a.till_no and date = a.run_date),0),0) - (select sum(amount) as amount from pos_payment_details where run_date = a.run_date and till_no = a.till_no) as variance FROM ( select DISTINCT(till_no),run_date,op_code,sum(amount) as total,
CASE 
  WHEN pos_payment_details.type = 'ACC' THEN
    sum(amount)
  ELSE
    0
END as ACC,
CASE 
  WHEN pos_payment_details.type = 'CAPS' THEN
    sum(amount)
  ELSE
    0
END as CAPS,
CASE 
  WHEN pay_type.type = 'CRN' AND pos_payment_details.till_no = '99'
  THEN
    sum(amount)
  ELSE
    0
END as CRN,
CASE 
  WHEN pos_payment_details.type = 'MPS' THEN
    sum(amount)
  ELSE
    0
END as MPS,
CASE 
  WHEN pos_payment_details.type = 'VIS' THEN
    sum(amount)
  ELSE
    0
END as VIS,
CASE 
  WHEN pay_type.type='PCS' THEN
    sum(amount)
  ELSE
    0
END as p_cash,
CASE 
  WHEN pay_type.cash = 'f' THEN
    sum(amount)
  ELSE
    0
END as non_cash


from pos_payment_details
LEFT JOIN pay_type ON pos_payment_details.type = pay_type.type 
where pos_payment_details.run_date ='$date' GROUP BY run_date,till_no,pos_payment_details.type,pay_type.type,cash,pos_payment_details.op_code,amount) as a
GROUP BY a.till_no,a.op_code,a.run_date ORDER BY till_no ASC;
") or die(pg_last_error($conn));
                  while ($pos = pg_fetch_assoc($qury)) {
                 

                 ?>
                     <tr>
                      <td><?php echo $pos['till_no'] ?></td>
                      <td><?php echo $pos['op_code'] ?></td>
                      <td><?php echo number_format($pos['total'],2) ?></td>
                      <td><?php echo number_format($pos['cash_back'],2) ?></td>
                      <td><?php echo number_format($pos['net'],2) ?></td>
                      <td><?php echo number_format($pos['non_cash'],2) ?></td>
                      <td><?php echo number_format($pos['acc'],2) ?></td>
                      <td><?php echo number_format($pos['dep'],2) ?></td>
                      <td><?php echo number_format($pos['caps'],2) ?></td>
                      <td><?php echo number_format($pos['p_cash'],2) ?></td>
                      <td><?php echo number_format($pos['pickups'],2) ?></td>
                      <td><?php echo number_format($pos['mps'],2) ?></td>
                      <td><?php echo number_format($pos['vis'],2) ?></td>
                      <td style="background-color: lightgreen;"><?php echo number_format($pos['banking'],2) ?></td>
                      <td style="background-color: #ff6b6b;"><?php echo number_format($pos['variance'],2) ?></td>
                    </tr>

                    <?php
                  }
                }
              }
                     ?>
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                <table class="table table-sm ">
                  <thead>
                    <td>Customers</td>
                    <td>Gross sales</td>
                    <td>Cash backs</td>
                    <td>net sales</td>
                    <td>Non-cash</td>
                    <td>On Account</td>
                    <td>Deposits</td>
                    <td>Caps Paid</td>
                    <td>P/cash</td>
                    <td>Pickups</td>
                    <td>Credit Card</td>
                    <td>Mobile Money</td>
                  </thead>
                  <tbody>
                    <?php 
                    if (isset($_POST['refresh'])) {

                  $date = $_POST['date'];

                  $t_query = pg_query($conn, "SELECT * FROM till_trans where date = '$date'") or die(pg_last_error($conn)); $t_row = pg_num_rows($t_query); 

                    $results = pg_query($conn, "SELECT sum(data.sales) as gross_sales,sum(data.cash_back) as cash_backs,sum(data.net_sales) as net_sales,sum(data.non_cash) as non_cash,sum(data.on_acc) as on_account,sum(data.deposits) as deposits,sum(data.caps) as caps,sum(data.p_cash) as petty,sum(data.pickups) as pickups,sum(data.credit) as credit_cards,sum(data.mpesa) as m_money,sum(data.variance) as variance FROM (SELECT DISTINCT(a.till_no),a.op_code,sum(a.total_sales) as sales,sum((COALESCE(deposits,0)+COALESCE(caps,0)+COALESCE(on_acc,0)) + a.total_sales ) as net_sales,COALESCE(sum(a.pickups),0) as pickups,sum(a.non_cash) as non_cash,COALESCE(sum(a.refund),0) as cash_back,sum(a.deposits) as deposits, sum(a.on_acc) as on_acc,sum(a.caps)as caps,sum(a.cash) as cash,sum(a.check) as check,sum(a.credit) as credit,sum(a.gift) as gift,sum(a.loyalty) as loyalty,sum(a.mpesa) as mpesa,sum(a.visa) as visa,COALESCE(sum(a.p_cash),0) as p_cash,sum(a.mpesa - a.total_sales) as variance FROM (SELECT DISTINCT(pay_trans_details.till_no),pay_trans_details.op_code, COALESCE(sum(pay_amount),0) as total_sales,COALESCE(sum(pickup_trans.amount),0) as pickups,
CASE 
  WHEN pay_type.type='RFD' THEN
    sum(pay_amount)
  ELSE
    0
END as refund,
CASE 
  WHEN pay_type.cash='f' THEN
    sum(pay_amount)
  ELSE
    0
END as non_cash,
CASE 
  WHEN pay_type.type='PCS' THEN
    sum(pay_amount)
  ELSE
    0
END as p_cash,
CASE 
  WHEN pay_type='DEP' THEN
    sum(pay_amount)
  ELSE
    0
END as deposits,
CASE 
  WHEN pay_type='ACC' THEN
    sum(pay_amount)
  ELSE
    0
END as on_acc,CASE 
  WHEN pay_type='CAPS' THEN
    sum(pay_amount)
  ELSE
    0
END as caps,
CASE 
  WHEN pay_type='CAS' THEN
    sum(pay_amount)
  ELSE
    0
END as cash,
CASE 
  WHEN pay_type='CHQ' THEN
    sum(pay_amount)
  ELSE
    0
END as check,
CASE 
  WHEN pay_type='CRN' THEN
    sum(pay_amount)
  ELSE
    0
END as credit,
CASE 
  WHEN pay_type='GFT' THEN
    sum(pay_amount)
  ELSE
    0
END as gift,
CASE 
  WHEN pay_type='LOY' THEN
    sum(pay_amount)
  ELSE
    0
END as loyalty,
CASE 
  WHEN pay_type='MPS' THEN
    sum(pay_amount)
  ELSE
    0
END as mpesa,
CASE 
  WHEN pay_type='VIS' THEN
    sum(pay_amount)
  ELSE
    0
END as visa
from pay_trans_details
left join pickup_trans on pay_trans_details.till_no = pickup_trans.till
LEFT JOIN pay_type ON pay_trans_details.pay_type = pay_type.type
where pay_trans_details.date = '$date'
group by till_no,pay_trans_details.op_code,pay_type,cash,pay_type.type) as a
group by till_no,op_code
order by till_no) as data;
") or die(pg_last_error($conn));
                    $total = pg_fetch_assoc($results);


}else{
 return 0;
}
                     ?>
                    <tr>
                    <td><input type="text" name="custs" style="width: 65px; background-color: #FFFACD;" value="<?php echo $t_row ?>" ></td>
                    <td><input type="text" name="gross" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['gross_sales'],2) ?>" readonly></td>
                    <td><input type="text" name="cash" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['cash_backs'],2) ?>" readonly></td>
                    <td><input type="text" name="netsales" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['net_sales'],2) ?>" readonly></td>
                    <td><input type="text" name="noncash" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['non_cash'],2) ?>" readonly></td>
                    <td><input type="text" name="onaccount" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['on_account'],2) ?>" readonly></td>
                    <td><input type="text" name="deposits" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['deposits'],2) ?>" readonly></td>
                    <td><input type="text" name="caps" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['caps'],2) ?>" readonly></td>
                    <td><input type="text" name="p/cash" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['petty'],2) ?>" readonly></td>
                    <td><input type="text" name="pickups" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['pickups'],2) ?>" readonly></td>
                    
                    <td><input type="text" name="credit" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['credit_cards'],2) ?>" readonly></td>
                    <td><input type="text" name="m/money" style="width: 65px; background-color: #FFFACD;" value="<?php echo number_format($total['m_money'],2) ?>"  readonly></td>
                    
                  </tr>
                  </tbody>
                </table>
                <div class="row col-12">
                  <div class="col-8">
                <address>
                  <i>
                    Banking = pickups<br>
                    Net sales = total sales - cash backs<br>
                    Variance = net sales + deposits + on account + caps - pickups -refunds - expenses - p/cash - Non-Cash payments
                  </i>
                </address>
              </div>
                <div class="col-4">
                  <div class="float-right">
                  Banking: 
                  <input type="text" name="" style=" text-align:  right; background-color: lightgreen;" value="<?php echo number_format($total['m_money'],2) ?>" readonly>
                  <br>

                  Variance: 
                  <input type="text" name="var" style="text-align:  right; background-color: #ff6b6b;" value="<?php echo number_format($total['variance'],2) ?>" readonly>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
