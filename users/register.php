<?php  
session_start();

require '../config/connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $_SESSION['company']['name'] ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">

  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="#"><b>Smart</b>POS</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>
      <form method="post" id="register-user">
        <?php
        $qq = pg_query($conn, "SELECT * FROM system_master WHERE item='user';");
        $ff = pg_fetch_array($qq);
        $lno=intval($ff['last_number'])+1;
        
        $jj = pg_query($conn, "SELECT * FROM company");
        $qq = pg_fetch_array($jj);
        $user_id = $qq['branch'];

        $ln = $user_id.$lno;
      ?>

      <div id="message"></div>

          <div class="input-group mb-3">
          <input type="id" class="form-control" placeholder="User Id" value="<?php echo $ln ?>" id="id" name="id" readonly required="">
        </div>

        <div class="input-group mb-3">
          <input type="Username" class="form-control" placeholder="Username" id="username" name="username" required="">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Full name" id="name"  name="name"  required="">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password" required="">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Retype password" id="confrim" name="confirm" required="">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" required>
              <label for="agreeTerms">
               I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-sm btn-wave btn-block btn-primary" name="register" id="register">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="../login.php" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

</body>
</html>

<?php

if(isset($_POST['register'])){

extract($_POST);
$branch = $_SESSION['company']['branch'];
/*$id = $_POST['id'];
$username = $_POST['username'];
$name = $_POST['name'];
$password = $_POST['password'];
$confirm = $_POST['confirm'];*/

if ($password != $confirm) {
  echo "<script language='javascript'>alert('Password Mismatch !');</script>";
	exit;
}

$chk = pg_query($conn, "SELECT * FROM sys_user WHERE username = '$username' and id !='$id' ") or die(pg_last_error($conn));
$chq = pg_num_rows($chk);
		if($chq > 0){
      echo "<script language='javascript'>alert('Username already exists !');</script>";
			exit;
		}
$row = pg_fetch_assoc($chk);
		if($row[id] != $id){
			$save = pg_query($conn, "INSERT INTO sys_user(id, username, name, password,branch,sys_module,blocked,admin,super_user) VALUES ('$id','$username','$name',MD5('$password'),'$branch','BOTH', 'f', 't', 't')") or die(pg_last_error($conn));
		}else{
			$save = pg_query($conn, "UPDATE sys_user set username='$username', name='$name', password=MD5('$password'), branch = '$branch', sys_module = 'BOTH', blocked = 'f', admin = 't', super_user = 't' where id = ".$id) or die(pg_last_error($conn));
		}
		if($save){
			pg_query($conn, "UPDATE system_master SET last_number = last_number+1 WHERE item = 'user'") or die(pg_last_error($conn));
			header('location:../login.php');
		}
	}

?>
