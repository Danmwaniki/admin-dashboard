<?php 
session_start();

include 'config/connection.php';
 if (isset($_GET['id'])) {

   $doc_no = $_GET['id'];
   $run_date = $_SESSION['rec_date'];

 $qry = "SELECT company.name as company,branch.name as branch,address,phone,pin_no,vat_no from company left join branch on branch.code = company.branch";
  $result = pg_query($conn, $qry) or die (pg_last_error($conn));
  $row = pg_fetch_assoc($result);


  $query = "SELECT DISTINCT(pos_trans_details.op_code),sys_user.name,pos_trans_details.till_no,pos_payment_details.type as mode,pos_trans_details.input_date AS rundate from pos_trans_details 
                            LEFT JOIN sys_user ON sys_user.operator_code = pos_trans_details.op_code 
                            left join pos_payment_details on pos_trans_details.doc_no=pos_payment_details.receipt_no
                            WHERE doc_no = '$doc_no' and pos_trans_details.run_date = '$run_date' GROUP BY pos_trans_details.op_code,sys_user.name,pos_trans_details.till_no,mode,pos_trans_details.input_date
                            UNION
                            SELECT DISTINCT(till_trans_details.op_code),sys_user.name,till_trans_details.till_no,till_payment_details.type as mode,till_trans_details.input_date AS rundate from till_trans_details 
                            LEFT JOIN sys_user ON sys_user.operator_code = till_trans_details.op_code 
                            left join till_payment_details on till_trans_details.doc_no=till_payment_details.receipt_no
                            WHERE doc_no = '$doc_no' and till_trans_details.run_date = '$run_date' GROUP BY till_trans_details.op_code,sys_user.name,till_trans_details.till_no,mode,till_trans_details.input_date";
        $results = pg_query($conn, $query) or die(pg_last_error($conn));
        $rw = pg_fetch_assoc($results);



$qury = "SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,pos_trans_details.input_date FROM pos_trans_details LEFT JOIN packing on pos_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date' UNION SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,till_trans_details.input_date FROM till_trans_details LEFT JOIN packing on till_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date'";
$rslt = pg_query($conn, $qury) or die(pg_last_error($conn));
$i = 1;
$rows = pg_num_rows($rslt);

$t_query = "SELECT sum(a.unit_incl) as cost, sum(a.total_incl) as total from(SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,pos_trans_details.input_date FROM pos_trans_details LEFT JOIN packing on pos_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date' UNION SELECT doc_no,code,description,qty,packing.pack_name as uom,unit_incl,discount,total_incl,till_trans_details.input_date FROM till_trans_details LEFT JOIN packing on till_trans_details.uom::integer = packing.pack_id WHERE doc_no = '$doc_no' and run_date='$run_date') as a;";
$t_result = pg_query($conn, $t_query) or die(pg_last_error($conn));
$t_row = pg_fetch_assoc($t_result);

}
?>

<!DOCTYPE html>
 <html>
 <head>
   <meta charset="utf-8">
   <title>Printing....</title>

 <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

   
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<style type="text/css">
  .rec{
    margin-left: 10rem;
    margin-top: 3rem;
    width: 80%;
  }
  h3{
    text-align: center;
  }
  input{
    font-size: medium;
    padding: 0px;
    background-color: #e5e4e2;
    border-width: 1px;
    border-color: #c0c0c0;
    font-weight: lighter;
  }
    .header{
    text-align: center;
  }
</style>
 
 </head>
<body class="hold-transition" style="background-color: #dcdcdc;">
<div class="wrapper">

 <!-- Content Wrapper. Contains page content -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="rec">
            <div class="card">
              <div class="card-header">
                 <div class="header">
                    <h2><b><?php echo $row['company']; ?></b></h2>
                    <h5><?php echo $row['address'] ?>, TEL: <?php echo $row['phone'] ?>, <b><?php echo $row['branch'] ?></b>,</h5>
                    <h5>CUSTOMER RECEIPT</h5>
                    </div>
                  <!--  info row -->
                   <div class="row invoice-info">
                  
                  <div class="col-sm-7 invoice-col" align="left">
                    <address>
                        RECEIPT NO : <?php echo $doc_no ?><br>
                        TILL NO :<?php echo $rw['till_no'] ?><br>
                        OPERATOR : <?php echo $rw['op_code'] ?>, <?php echo $rw['name'] ?><br> 
                    </address>
                  </div>
                    <div class="col-sm-4 invoice-col" align="right">
                      <address>
                      DATE: <?php echo $rw['rundate'] ?><br>
                      Payment Mode:  <?php echo $rw['mode'] ?><br>
                     </address>
                   </div>
                  </div>
              </div>
              <div class="card-body">
                <table class="table table-sm table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <td>No:</td>
                      <td>Scancode:</td>
                      <td>Description:</td>
                      <td>Qty:</td>
                      <td>@Price:</td>
                      <td>Amount:</td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php while ($d_row = pg_fetch_assoc($rslt)) {
                      ?>
                    <tr>
                      <td><?php echo $i++ ?></td>
                      <td><?php echo $d_row['code'] ?></td>
                      <td><?php echo $d_row['description'] ?></td>
                      <td><?php echo number_format($d_row['qty'],2) ?></td>
                      <td><?php echo number_format($d_row['unit_incl'],2) ?></td>
                      <td><?php echo number_format($d_row['total_incl'],2) ?></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <div class="card-footer">
                <div class="float-sm-left">
                <b><?php echo $rows ?> Items</b>
              </div>
              <div class="float-sm-right"><b>
                <table class="table table-sm table-striped table-hover">
                  <th>
                Total Cost: 
                <input type="text" value="<?php echo number_format($t_row['cost'],2) ?>" name="" style="border-width: 0px; text-align: right; width: 90px; background-color: inherit;" readonly>
              </th>
              <td>
                Total Amount: 
                <input type="text" value="<?php echo number_format($t_row['total'],2) ?>" name="" style="border-width: 0px; text-align: right; width: 90px; background-color: inherit;" readonly>
              </td>
            </table>
              </b>
              </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </div>
  

<!-- Page specific script -->
<script>
  window.addEventListener("load", window.print());
</script>
</body>
 <!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->

</body>
 </html>

